@extends('layouts.myview')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>{{ucfirst($title)}}</h4>
                <div class="card-header-action">
                    <a href="{{ route('dataObat.create') }}" class="btn btn-primary">{{__('Tambah Data Pemakaian Obat')}}</a>
                    <a href="{{ route('obat.index') }}" class="btn btn-danger">Kembali </a>
                </div>
            </div>
            <div class="card-body">
                <form action="#" method="post" id="form-peramalan">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label>{{__('Obat')}}</label>
                                <select name="obat" id="obat" class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="form-group">
                                <label>{{__('Periode Awal ')}}</label>
                                <input type="text" name="awal" id="awal" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="form-group">
                                <label>{{__('Periode Akhir')}}</label>
                                <input type="text" name="akhir" id="akhir" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row float-right">
                        <button type="submit" id="submit-btn" class="btn btn-primary mr-2">{{__("Hitung")}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card" id="show-data">
            <div class="card-body">
                <div class="empty-state" data-height="400">
                    <div class="empty-state-icon">
                        <i class="fas fa-check"></i>
                    </div>
                    <h2>Hasil Peramalan</h2>
                    <div id="field">
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                        <a href="#" id="redirect-to-ingoing" class="btn btn-primary mr-2">{{__("Akses Ke Stok Obat Masuk")}}</a>
                        {{-- <a href="{{ route('ingoing.create') }}" id="redirect-to-ingoing" class="btn btn-primary mr-2">{{__("Akses Ke Stok Obat Masuk")}}</a> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-6">
        <div class="card" id="card-1">
            <div class="card-header">
                <h4>{{__("Mape 1 =")}}</h4>
                <h4 id="mape-1">{{__("0")}}</h4>
                <div class="card-header-action">
                    <a data-collapse="#card-collapse-1" class="btn" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse" id="card-collapse-1">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table-1">
                            <thead>
                                <th scope="col">{{(__('No'))}}</th>
                                <th scope="col">{{(__('X'))}}</th>
                                <th scope="col">{{(__('F'))}}</th>
                                <th scope="col">{{(__('e'))}}</th>
                                <th scope="col">{{(__('E'))}}</th>
                                <th scope="col">{{(__('EA'))}}</th>
                                <th scope="col">{{(__('alpha'))}}</th>
                                <th scope="col">{{(__('beta'))}}</th>
                                <th scope="col">{{(__('PE'))}}</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="card" id="card-2">
            <div class="card-header">
                <h4>{{__("Mape 2 =")}}</h4>
                <h4 id="mape-2">{{__("0")}}</h4>
                <div class="card-header-action">
                    <a data-collapse="#card-collapse-2" class="btn" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse" id="card-collapse-2">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table-2">
                            <thead>
                                <th scope="col">{{(__('No'))}}</th>
                                <th scope="col">{{(__('X'))}}</th>
                                <th scope="col">{{(__('F'))}}</th>
                                <th scope="col">{{(__('e'))}}</th>
                                <th scope="col">{{(__('E'))}}</th>
                                <th scope="col">{{(__('EA'))}}</th>
                                <th scope="col">{{(__('alpha'))}}</th>
                                <th scope="col">{{(__('beta'))}}</th>
                                <th scope="col">{{(__('PE'))}}</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="card" id="card-3">
            <div class="card-header">
                <h4>{{__("Mape 3 =")}}</h4>
                <h4 id="mape-3">{{__("0")}}</h4>
                <div class="card-header-action">
                    <a data-collapse="#card-collapse-3" class="btn" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse" id="card-collapse-3">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table-3">
                            <thead>
                                <th scope="col">{{(__('No'))}}</th>
                                <th scope="col">{{(__('X'))}}</th>
                                <th scope="col">{{(__('F'))}}</th>
                                <th scope="col">{{(__('e'))}}</th>
                                <th scope="col">{{(__('E'))}}</th>
                                <th scope="col">{{(__('EA'))}}</th>
                                <th scope="col">{{(__('alpha'))}}</th>
                                <th scope="col">{{(__('beta'))}}</th>
                                <th scope="col">{{(__('PE'))}}</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="card" id="card-4">
            <div class="card-header">
                <h4>{{__("Mape 4 =")}}</h4>
                <h4 id="mape-4">{{__("0")}}</h4>
                <div class="card-header-action">
                    <a data-collapse="#card-collapse-4" class="btn" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse" id="card-collapse-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table-4">
                            <thead>
                                <th scope="col">{{(__('No'))}}</th>
                                <th scope="col">{{(__('X'))}}</th>
                                <th scope="col">{{(__('F'))}}</th>
                                <th scope="col">{{(__('e'))}}</th>
                                <th scope="col">{{(__('E'))}}</th>
                                <th scope="col">{{(__('EA'))}}</th>
                                <th scope="col">{{(__('alpha'))}}</th>
                                <th scope="col">{{(__('beta'))}}</th>
                                <th scope="col">{{(__('PE'))}}</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="card" id="card-5">
            <div class="card-header">
                <h4>{{__("Mape 5 =")}}</h4>
                <h4 id="mape-5">{{__("0")}}</h4>
                <div class="card-header-action">
                    <a data-collapse="#card-collapse-5" class="btn" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse" id="card-collapse-5">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table-5">
                            <thead>
                                <th scope="col">{{(__('No'))}}</th>
                                <th scope="col">{{(__('X'))}}</th>
                                <th scope="col">{{(__('F'))}}</th>
                                <th scope="col">{{(__('e'))}}</th>
                                <th scope="col">{{(__('E'))}}</th>
                                <th scope="col">{{(__('EA'))}}</th>
                                <th scope="col">{{(__('alpha'))}}</th>
                                <th scope="col">{{(__('beta'))}}</th>
                                <th scope="col">{{(__('PE'))}}</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="card" id="card-6">
            <div class="card-header">
                <h4>{{__("Mape 6 =")}}</h4>
                <h4 id="mape-6">{{__("0")}}</h4>
                <div class="card-header-action">
                    <a data-collapse="#card-collapse-6" class="btn" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse" id="card-collapse-6">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table-6">
                            <thead>
                                <th scope="col">{{(__('No'))}}</th>
                                <th scope="col">{{(__('X'))}}</th>
                                <th scope="col">{{(__('F'))}}</th>
                                <th scope="col">{{(__('e'))}}</th>
                                <th scope="col">{{(__('E'))}}</th>
                                <th scope="col">{{(__('EA'))}}</th>
                                <th scope="col">{{(__('alpha'))}}</th>
                                <th scope="col">{{(__('beta'))}}</th>
                                <th scope="col">{{(__('PE'))}}</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="card" id="card-7">
            <div class="card-header">
                <h4>{{__("Mape 7 =")}}</h4>
                <h4 id="mape-7">{{__("0")}}</h4>
                <div class="card-header-action">
                    <a data-collapse="#card-collapse-7" class="btn" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse" id="card-collapse-7">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table-7">
                            <thead>
                                <th scope="col">{{(__('No'))}}</th>
                                <th scope="col">{{(__('X'))}}</th>
                                <th scope="col">{{(__('F'))}}</th>
                                <th scope="col">{{(__('e'))}}</th>
                                <th scope="col">{{(__('E'))}}</th>
                                <th scope="col">{{(__('EA'))}}</th>
                                <th scope="col">{{(__('alpha'))}}</th>
                                <th scope="col">{{(__('beta'))}}</th>
                                <th scope="col">{{(__('PE'))}}</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="card" id="card-8">
            <div class="card-header">
                <h4>{{__("Mape 8 =")}}</h4>
                <h4 id="mape-8">{{__("0")}}</h4>
                <div class="card-header-action">
                    <a data-collapse="#card-collapse-8" class="btn" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse" id="card-collapse-8">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table-8">
                            <thead>
                                <th scope="col">{{(__('No'))}}</th>
                                <th scope="col">{{(__('X'))}}</th>
                                <th scope="col">{{(__('F'))}}</th>
                                <th scope="col">{{(__('e'))}}</th>
                                <th scope="col">{{(__('E'))}}</th>
                                <th scope="col">{{(__('EA'))}}</th>
                                <th scope="col">{{(__('alpha'))}}</th>
                                <th scope="col">{{(__('beta'))}}</th>
                                <th scope="col">{{(__('PE'))}}</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="card" id="card-9">
            <div class="card-header">
                <h4>{{__("Mape 9 =")}}</h4>
                <h4 id="mape-9">{{__("0")}}</h4>
                <div class="card-header-action">
                    <a data-collapse="#card-collapse-9" class="btn" href="#"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="collapse" id="card-collapse-9">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-striped" id="table-9">
                            <thead>
                                <th scope="col">{{(__('No'))}}</th>
                                <th scope="col">{{(__('X'))}}</th>
                                <th scope="col">{{(__('F'))}}</th>
                                <th scope="col">{{(__('e'))}}</th>
                                <th scope="col">{{(__('E'))}}</th>
                                <th scope="col">{{(__('EA'))}}</th>
                                <th scope="col">{{(__('alpha'))}}</th>
                                <th scope="col">{{(__('beta'))}}</th>
                                <th scope="col">{{(__('PE'))}}</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js')
    <script>
        var beta = '';
        var dataTarget = '';
        var dataMape = '';

        $(document).ready(function() {
            // getBulan();
            // getTahun();
            $("#awal").datepicker({
                autoclose: true,
                format: "mm-yyyy",
                viewMode: "months", 
                minViewMode: "months",
                onSelect: function(selectedDate) {
                    $('#awal').text();
                }
            });
            $("#akhir").datepicker({
                autoclose: true,
                format: "mm-yyyy",
                viewMode: "months", 
                minViewMode: "months",
                onSelect: function(selectedDate) {
                    $('#akhir').text();
                }
            });
            getObat();
            hideCards(); 
        });

        function getTahun() {
            var url = '{{ route("admin.forecasting.getDataTahun") }}';
            var select = $('#tahun');
            const a = '<option value=null>Pilih Tahun</option>'

            $.get(url, function(data) {
                select.empty();
                select.append(a)
                $.each(data, function(key, value) {
                    select.append('<option value=' + value + '>' + value + '</option>');
                });
            });
        }

        function getObat() {
            var url = '{{ route("admin.forecasting.getDataObat") }}';
            var select = $('#obat');
            const a = '<option value=null>Pilih Obat</option>'
            
            $.get(url, function(data) {
                select.empty();
                select.append(a);
                $.each(data, function(key, value) {
                    select.append('<option value=' + value.id + '>' + value.name + ' ' + value.tipe.name + '</option>');
                })
            });
        }

        function getBulan() {
            var url = '{{ route("admin.forecasting.getDataBulan") }}';
            var select = $('#bulan');
            const a = '<option value=null>Pilih Bulan</option>'

            $.get(url, function(data) {
                select.empty();
                select.append(a)
                $.each(data, function(key, value) {
                    select.append('<option value=' + value + '>' + value + '</option>');
                });
            });
        }

        $("#form-peramalan").submit(function (e) { 
            e.preventDefault();
            
            hideCards()

            var obat =  $("#obat option:selected").val()
            var awal = $("#awal").datepicker('getDate')
            var akhir = $("#akhir").datepicker('getDate')

            if (obat == 'null' || awal == null || akhir == null) {
                checkInput(obat, awal, akhir)
                return false
            }

            var awalVal = awal.getMonth() +' '+awal.getYear()
            var akhirVal = akhir.getMonth() +' '+akhir.getYear()

            var url = '{{ route("admin.forecasting.calculateForecasting") }}';
            var data = {
                '__token': "{{ csrf_token() }}",
                'obat': obat,
                'awal': awal,
                'akhir': akhir,
            };
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(data) {
                    if (data.result != 0) {
                        beta = data.result.length;
                        dataTarget = data.result
                        dataMape = data.mape
                        dataShow = data.show_data
                        for (let i = 0; i < beta; i++) {
                            showData(i, dataTarget[i]);
                            showMape(i, dataMape[i]);
                        }
                        setTimeout(function() {
                            showResult(dataShow)
                        }, 1000);
                        toastEvent('Hasil!', data.message, 'topRight', 'success')  
                        $('#submit-btn').attr('disabled', true)
                        $('#submit-btn').html('<span class="spinner-border spinner-border-sm" id="spinner-btn" role="status" aria-hidden="true"></span> Loading...')
                    } else {
                        toastEvent('Error!', data.message, 'topRight', 'error')   
                    }
                }
            });
        });

        function error_msg(data) {
            iziToast.error({
                title: data[0],
                message: data[1],
                position: data[2],
            })
        }

        function checkInput(obat,awal,akhir) { 
            var response = []
            var position = 'topRight'
            var title = 'Input Error'

            if (obat == 'null' && awal == null && akhir == null) {
                var text = 'input obat, Periode Awal, dan Periode Akhir kosong'
            } else if (obat == 'null') {
                var text = 'input obat kosong'
            } else if (awal == null) {
                var text = 'input Periode Awal kosong'
            } else if (akhir == null) {
                var text = 'input Periode Akhir kosong'
            }
            
            response.push(title)
            response.push(text)
            response.push(position)
            
            error_msg(response)
        }

        function hideCards() {
            $("#card-1").hide();
            $("#card-2").hide();
            $("#card-3").hide();
            $("#card-4").hide();
            $("#card-5").hide();
            $("#card-6").hide();
            $("#card-7").hide();
            $("#card-8").hide();
            $("#card-9").hide();
            $("#show-data").hide();
            $("#reset-button").hide();
        }

        function showData(i, data) {
            $("#card-"+(i+1)).show();
            
            var total = data.length
            var markup = '';
            var tbody = $("#table-"+(i+1)+" tbody")
            
            tbody.empty()
            for (let j = 0; j < total; j++) {
                markup = "<tr><td scope='row'>"+(j+1)+"</td><td>"+data[j]["X"]+"</td><td>"+data[j]["F"]+"</td><td>"+data[j]["e"]+"</td><td>"+data[j]["E"]+"</td><td>"+data[j]["EA"]+"</td><td>"+data[j]["alpha"]+"</td><td>"+data[j]["beta"]+"</td><td>"+data[j]["PE"]+"</td></tr>"
                tbody.append(markup);
            }
        }
        
        function showResult(data) {
            $("#show-data").show()

            console.log(data);

            var markup = ''
            var field = $("#field")
            field.empty()
            // markup = "<h2>Hasil akhir dari Metode Arsess bulan "+data.bulan+" tahun "+data.tahun+" adalah "+data.hasil+"</h2>"
            markup = "<h2>Metode peramalan dengan MAPE terkecil adalah <strong>"+data.beta+"</strong> </h2> <h2> Hasil peramalan bulan <strong>"+data.bulan+"</strong> tahun <strong>"+data.tahun+"</strong> adalah <strong>"+data.hasil+"</strong></h2>"
            
            field.append(markup)
            $('#submit-btn').attr('disabled', false)
            $('#submit-btn').html('Hitung')
            $("#spinner-btn").remove();
        }

        function toastEvent(title, message, position, type) {
            if (type == 'error') {
                iziToast.error({
                    title: title,
                    message: message,
                    position: position
                });   
            } else {
                iziToast.success({
                    title: title,
                    message: message,
                    position: position
                });
            }
        }

        function showMape(i, data) {
            $('#mape-'+(i+1)).text(data);
        }
    </script>
@endpush
@endsection
@include('import.select2')
@include('import.datepicker')