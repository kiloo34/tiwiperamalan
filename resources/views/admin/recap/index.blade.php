@extends('layouts.myview')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>{{__('Filter Data')}}</h4>
            </div>
            <form action="#" method="post" id="form-search">
                <div class="card-body"> 
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <label>{{__('Obat')}}</label>
                                <select name="obat" id="obat" class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <label>{{__('Bulan')}}</label>
                                <input type="text" name="awal" id="awal" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-success">Cari ...</button>
                    {{-- <input type="submit" value="Cari" class="btn btn-success float-right"> --}}
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 ">
        <div class="card">
            <div class="card-header">
                <h4>Daftar {{ucwords($title)}}</h4>
            </div>
            <div class="card-body">
                <div class="container-fluid">
                    <div class="row">
                        
                    </div>
                    <div class="row">
                        <div class="table">
                            <table id="obat-table" class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th rowspan="2">{{__('No')}}</th>
                                        <th rowspan="2">{{__('Nama')}}</th>
                                        {{-- <th rowspan="2">{{__('Harga Satuan')}}</th> --}}
                                        <th colspan="1">{{__('Mutasi Masuk')}}</th>
                                        <th colspan="1">{{__('Mutasi Keluar')}}</th>
                                        {{-- <th rowspan="2">{{__('Sisa Stock')}}</th> --}}
                                    </tr>
                                    <tr>
                                        <th colspan="1">{{__('Unit')}}</th>
                                        {{-- <th colspan="2">{{__('Jumlah')}}</th> --}}
                                        <th colspan="1">{{__('Unit')}}</th>
                                        {{-- <th colspan="2">{{__('Jumlah')}}</th> --}}
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    var table = $('#obat-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "{{ route('admin.rekap.getData') }}",
          data: function (d) {
                d.obat = $('#obat').val(),
                d.awal = $('#awal').val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'unitmasuk', name: 'unitmasuk'},
            // {data: 'jumlahmasuk', name: 'jumlahmasuk'},
            {data: 'unitkeluar', name: 'unitkeluar'},
            // {data: 'jumlahkeluar', name: 'jumlahkeluar'},
            // {data: 'sisa', name: 'sisa'},
        ]
    });
    $(document).ready(function() {
        getObat();
        $("#obat").select2();
        $("#awal").datepicker({
            autoclose: true,
            format: "mm-yyyy",
            viewMode: "months", 
            minViewMode: "months",
            onSelect: function(selectedDate) {
                $('#awal').text();
            }
        });
        $('#obat-table').DataTable();
    });

    $('#form-search').submit(function(e){
        e.preventDefault()
        table.draw();
    });

    function getObat() {
        var url = '{{ route("admin.getObat") }}';
        $.get(url, function(data) {
            var select = $('#obat');
            select.append('<option value="null">Pilih Obat</option>')
            $.each(data, function(key, value) {
                select.append('<option value=' + value.id + '>' + value.name + '</option>');
            });
            // select.select2("val", "");
        });
    }
</script>
@endpush
@include('import.datatable')
@include('import.select2')
@include('import.datepicker')

{{-- <tbody>
    <?php $no=1 ?>
    @foreach ($rekap as $r)
    @if (count($r->detail_transaksi) == 1)
        <tr>
            <td>{{$no}}</td>
            <td>{{$r->detail_transaksi[0]->obat->name}} {{$r->detail_transaksi[0]->obat->tipe->name}} {{$r->detail_transaksi[0]->obat->weight}}</td>
            @if ($r->type_id == 1)
            <td>{{$r->detail_transaksi[0]->quantity}}</td>
            <td>{{$r->detail_transaksi[0]->price}}</td>
            <td> - </td>
            <td> - </td>
            <td>0</td>
            @elseif ($r->type_id == 2)
            <td> - </td>
            <td> - </td>
            <td>{{$r->detail_transaksi[0]->quantity}}</td>
            <td>{{$r->detail_transaksi[0]->price}}</td>
            <td>0</td>
            @endif
            <?php $no++ ?>
        </tr>
    @else
        @for ($i = 0; $i < count($r->detail_transaksi); $i++)
        <tr>
            <td>{{$no}}</td>
            <td>{{$r->detail_transaksi[$i]->obat->name}} {{$r->detail_transaksi[$i]->obat->tipe->name}} {{$r->detail_transaksi[$i]->obat->weight}}</td>
            @if ($r->type_id == 1)
            <td>{{$r->detail_transaksi[$i]->quantity}}</td>
            <td>{{$r->detail_transaksi[$i]->price}}</td>
            <td> - </td>
            <td> - </td>
            <td>0</td>
            @elseif ($r->type_id == 2)
            <td> - </td>
            <td> - </td>
            <td>{{$r->detail_transaksi[$i]->quantity}}</td>
            <td>{{$r->detail_transaksi[$i]->price}}</td>
            <td>0</td>
            @endif
            <?php $no++ ?>
        </tr>
        @endfor
    @endif
    @endforeach
</tbody> --}}