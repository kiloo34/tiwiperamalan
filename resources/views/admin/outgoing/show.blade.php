@extends('layouts.myview')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>{{__('Order Status')}}</h4>
            </div>
            <div class="card-body">
                <div class="row mt-4">
                    <div class="col-12 col-lg-8 offset-lg-2">
                        <div class="wizard-steps">
                            @if ($order->transaksi->status_id == 1)
                                @include('components.wizard.placed')
                            @elseif ($order->transaksi->status_id == 2)
                                @include('components.wizard.placed')
                                @include('components.wizard.payment')
                            @elseif ($order->transaksi->status_id == 3)
                                @include('components.wizard.placed')
                                @include('components.wizard.payment')
                                @include('components.wizard.shipped')
                            @elseif ($order->transaksi->status_id == 4)
                                @include('components.wizard.placed')
                                @include('components.wizard.payment')
                                @include('components.wizard.shipped')
                                @include('components.wizard.status.complete')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 ">
        <div class="card">
            <div class="card-header">
                <h4>Detail Data {{$order->obat->name}}</h4>
                <div class="card-header-action">
                    {{-- <a href="{{ route('order.create') }}" class="btn btn-primary">{{__('Tambah Data ')}} {{$title}} </a> --}}
                </div>
            </div>
            <div class="card-body">
                
            </div>

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>

</script>
@endpush