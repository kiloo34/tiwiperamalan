@extends('admin.outgoing.create')
@section('page_title')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>{{ucfirst($title == 'outgoing' ? 'Obat Keluar' : $title)}}</h4>
                <div class="card-header-action">
                    <a href="{{ route('outgoing.index') }}" class="btn btn-danger">{{__('Kembali')}} </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@if ($data->status_id != 4)
@section('page_list_obat')
<div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4>{{__('Daftar Obat')}}</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-obat" class="table table-striped">
                        <thead>
                            <th>{{__('No')}}</th>
                            <th>{{__('Nama')}}</th>
                            <th>{{__('Stock')}}</th>
                            <th>{{__('Aksi')}}</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('page_list_keranjang')
    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4>
                    @if ($data!=null or '')
                    @if ($data->invoice_number != '-'  or  null)
                    {{__('Detail Data Obat')}}{{ $data->invoice_number }}
                    @else
                    {{__('Keranjang Obat')}} {{ $data->id }}
                    @endif
                    @endif
                </h4>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="table-responsive">
                        <table id="table-keranjang" class="table table-striped">
                            <thead>
                                <th>{{__('No')}}</th>
                                <th>{{__('Nama')}}</th>
                                <th>{{__('Kuantitas')}}</th>
                                <th>{{__('Aksi')}}</th>
                            </thead>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_action_button')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Selesaikan Transaksi</h4>
                <div class="card-header-action">
                    <a href="{{ route('outgoing.index') }}" class="btn btn-success" id="proses-obat" data-id="{{ $data->id }}">{{__('Proses Obat')}} </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@else

{{-- @section('page_list_keranjang')
<div class="row">
    <div class="col-12 ">
        <div class="card">
            <div class="card-header">
                <h4>
                    @if ($data!=null or '')
                    @if ($data->invoice_number != '-'  or  null)
                    {{__('Detail Data Obat')}} {{ $data->invoice_number }}
                    @else
                    {{__('Keranjang Obat')}} {{ $data->id }}
                    @endif
                    @endif
                </h4>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="table-responsive">
                        <table id="table-keranjang" class="table table-striped">
                            <thead>
                                <th>{{__('No')}}</th>
                                <th>{{__('Nama')}}</th>
                                <th>{{__('Kuantitas')}}</th>
                                <th>{{__('Aksi')}}</th>
                            </thead>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection --}}
@endif

{{-- @section('modal')
@foreach ($obat as $b)
<div class="modal fade" tabindex="-1" role="dialog" id="tambah-modal-{{$b->id}}">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{$b->name}} {{$b->tipe->name}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="" method="post" id="add-detail-form-{{$b->id}}">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="id_transaksi" id="id_transaksi" data-id="{{$data->id}}">
                    <input type="hidden" name="id_obat" id="id_obat" obat-id="{{$b->id}}">
                    <div class="form-group">
                        <label>{{__("Kuantitas")}}</label>
                        <input type="text" class="form-control" name="kuantitas" required">
                        @if ($errors->has('kuantitas'))
                            <span class="text-danger">{{ $errors->first('kuantitas') }}</span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="submit" class="btn btn-primary">tambah data</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@if ($data != null or '')
@foreach ($detail as $b)
<div class="modal fade" tabindex="-1" role="dialog" id="ubah-modal-{{$b->id}}">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{$b->obat->name}} {{$b->obat->tipe->name}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="form-group">
                        <label>{{__("Kuantitas")}}</label>
                        <input type="text" class="form-control" name="kuantitas" value="{{$b->quantity}}" required="">
                        @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('kuantitas') }}</span>
                        @endif
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-primary">ubah data</button>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif
@endsection --}}