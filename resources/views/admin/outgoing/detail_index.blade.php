@extends('admin.outgoing.index')
@section('modal')
@foreach ($order as $o)
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="show-{{$o->id}}-modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content container">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="table-{{$o->id}}-detail" class="table table-striped">
                                <thead>
                                    <th>{{__('No')}}</th>
                                    <th>{{__('Nama')}}</th>
                                    <th>{{__('Jumlah')}}</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection