@extends('layouts.myview')
@section('content')
@yield('page_title')
@yield('page_list_obat')
@yield('page_list_keranjang')
@yield('page_action_button')
@endsection

@push('scripts')
<script>
    $(document).ready(function(){
        $('#table-obat').DataTable({
            "language": {
                "emptyTable": "Data Obat Kosong"
            },
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('outgoing.getObat') }}",
            "columns": [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'stock', name: 'stock'}, 
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: false, 
                    searchable: false
                },
            ],
        });
        $('#table-keranjang').DataTable({
            "language": {
                "emptyTable": "Data Keranjang Kosong"
            },
            "processing": true,
            "serverSide": true,
            "ajax": "{{ route('outgoing.getKeranjang') }}",
            "columns": [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'stock', name: 'stock'}, 
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: false, 
                    searchable: false
                },
            ],
        });
        $('form').submit(function(e) {
            e.preventDefault();
            var transaksi_id = "{{ $data->id }}";
            var obat_id = $(this).val();

            console.log(transaksi_id, obat_id);

            var url = "{{ route('ingoing.destroy', [":transaction", ":medicine"]) }}";
            url = url.replace(':transaction', transaksi_id);
            url = url.replace(':medicine', obat_id);
            
        });
    })

    function addDetail(id) {
        var transaksi_id = "{{ $data->id }}";
        var obat_id = id;

        var url = "{{ route('outgoing.addCart', [":transaction", ":medicine"]) }}";
        url = url.replace(':transaction', transaksi_id);
        url = url.replace(':medicine', obat_id);

        var data = {'__token': "{{ csrf_token() }}"};

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data) {
                Swal.fire({
                    title: data.message,
                    icon: 'success',
                })
                console.log(data.obat, data.transaksi, data.cek, data.message);
                reloadTable('#table-obat', 100);
                reloadTable('#table-keranjang', 100);
            }
        });
    }

    function removeItem(id) {
        var transaksi_id = "{{ $data->id }}";
        var obat_id = id;

        var url = "{{ route('outgoing.deleteCartItem', [":transaction", ":medicine"]) }}";
        url = url.replace(':transaction', transaksi_id);
        url = url.replace(':medicine', obat_id);

        var data = {'__token': "{{ csrf_token() }}"};

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data) {
                Swal.fire({
                    title: data.message,
                    icon: 'success',
                })
                console.log(data.obat, data.transaksi, data.cek, data.message);
                reloadTable('#table-obat', 100);
                reloadTable('#table-keranjang', 100);
            }
        });
    }

    function updateItem(id) {
        var transaksi_id = "{{ $data->id }}";
        var obat_id = id;

        var url = "{{ route('outgoing.updateCartItem', [":transaction", ":medicine"]) }}";
        url = url.replace(':transaction', transaksi_id);
        url = url.replace(':medicine', obat_id);

        var data = {'__token': "{{ csrf_token() }}"};

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data) {
                Swal.fire({
                    title: data.message,
                    icon: 'success',
                })
                console.log(data.obat, data.transaksi, data.cek, data.message);
                reloadTable('#table-obat', 100);
                reloadTable('#table-keranjang', 100);
            }
        });
    }
    
    function reloadTable(selector, counter) {
        setTimeout(function() {
            $(selector).DataTable().ajax.reload();
        }, 100);
    }

    $('#proses-obat').on('click', function (e) {
        e.preventDefault();
        console.log(e);
    
        var id = $(this).data("id");
        var url = "{{ route('outgoing.update', ":id") }}";
        url = url.replace(':id', id);
        $object=$(this);

        console.log(url, id);

        Swal.fire({
            title: 'Are you sure?',
            text: "Selesaikan Data Order ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'PUT',
                    data: {id: id, '_method':'PUT'},
                    success: function (response) {
                        // $($object).parents('tr').remove();
                        Swal.fire({
                            title: "Data Diupdate!",
                            text: response.message,
                            icon: 'success',
                        });
                        // reloadPage(250);
                        location.href = "{{ route('outgoing.index') }}";
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR, textStatus, errorThrown);
                        Swal.fire({
                            title: "Data Gagal Diupdate!",
                            icon: 'error',
                        })
                    }
                });
            }
        });
    })    
</script>

@endpush

@include('import.datatable')
