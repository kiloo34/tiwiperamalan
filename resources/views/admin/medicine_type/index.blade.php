
@extends('layouts.myview')
@section('content')
{{-- <div class="row">
    <div class="col-md-6 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>Data Obat</h4>
            </div>
            <div class="card-body">
                <canvas id="chartharga"></canvas>
            </div>
        </div>
    </div>
</div> --}}
<div class="row">
    <div class="col-md-12 ">
        <div class="card">
            <div class="card-header">
                <h4>Daftar Tipe Obat</h4>
                <div class="card-header-action">
                    <a href="{{ route('tipe.create') }}" class="btn btn-primary">Tambah </a>
                    <a href="{{ route('obat.index') }}" class="btn btn-danger">Kembali </a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="tipe" class="table table-striped">
                        <thead>
                            <th>{{__('No')}}</th>
                            <th>{{__('Nama')}}</th>
                            <th>{{__('Aksi')}}</th>
                        </thead>
                        <tbody>
                            <?php $no=1 ?>
                            @foreach ($tipe as $t)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$t->name}}</td>
                                <td>
                                    <a href="{{ route('tipe.edit', $t->id) }}" class="btn btn-sm btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Ubah</a>
                                    <a href="{{ route('tipe.destroy', $t->id) }}"
                                        class="btn btn-sm btn-danger hapus-tipe" data-toggle="tooltip" data-placement="top"
                                        title="Hapus Data" data-id="{{ $t->id }}">
                                        <i class="fa fa-trash"></i> Hapus
                                    </a>
                                </td>
                                <?php $no++ ?>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#tipe').DataTable();
    });

    $('.hapus-tipe').on('click', function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var id = $(this).data("id");
        // var url = $('.hapus').attr('href');
        var url = "{{ route('tipe.destroy', ":id") }}";
        url = url.replace(':id', id);
        $object=$(this);

        Swal.fire({
            title: 'Are you sure?',
            text: "Yakin ingin menghapus Tipe Obat ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    data: {id: id},
                    success: function (response) {
                        $($object).parents('tr').remove();
                        Swal.fire({
                            title: "Data Dihapus!",
                            text: response.message,
                            icon: 'success',
                        });
                        location.reload();
                    },
                    error: function (data) {
                        Swal.fire({
                            title: "Data Gagal Dihapus!",
                            icon: 'error',
                        })
                    }
                });
            }
        });
    })
</script>
@endpush
{{-- @include('import.chartjs') --}}
@include('import.datatable')
{{-- var ctx2 = document.getElementById("chartpermintaan").getContext('2d');
        var myChart = new Chart(ctx2, {
            type: 'bar',
            data: {
                labels: @json($label),
                datasets: [
                    @foreach ($chart as $c)
                    {
                        label: @json(ucfirst($c['kecamatan'])) ,
                        data: @json($c['permintaan']),
                        borderColor: '#'+Math.floor(Math.random()*16777215).toString(16),
                        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
                        pointRadius: 4
                    },
                    @endforeach
                ]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: @json(ucfirst($title)),
                    }
                },
                legend: {
                    display: true
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    x: {
                        stacked: true,
                    },
                    y: {
                        stacked: true
                    }
                },
            },
        }); --}}

        {{-- $(document).ready(function() {
            var ctx1 = document.getElementById("chartharga").getContext('2d');
            var myChart = new Chart(ctx1, {
                type: 'bar',
                data: {
                    labels: @json($label),
                    datasets: [
                        @foreach ($chart as $c)
                        {
                            label: @json(ucfirst($c['kecamatan'])) ,
                            data: @json($c['harga']),
                            borderColor: '#'+Math.floor(Math.random()*16777215).toString(16),
                            backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
                            pointRadius: 4
                        },
                        @endforeach
                    ]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: @json(ucfirst($title)),
                        }
                    },
                    legend: {
                        display: true
                    },
                    interaction: {
                        intersect: false,
                        mode: 'index',
                    },
                    scales: {
                        x: {
                            stacked: true,
                        },
                        y: {
                            stacked: true
                        }
                    },
                },
            });
    
    
        }); --}}