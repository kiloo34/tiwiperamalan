@extends('layouts.myview')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>{{__('Order Status')}}</h4>
            </div>
            <div class="card-body">
                <div class="row mt-4">
                    <div class="col-12 col-lg-8 offset-lg-2">
                        <div class="wizard-steps">
                            @if ($detail->transaksi->status_id == 1)
                                @include('components.wizard.placed')
                            @elseif ($detail->transaksi->status_id == 2)
                                @include('components.wizard.placed')
                                @include('components.wizard.payment')
                            @elseif ($detail->transaksi->status_id == 3)
                                @include('components.wizard.placed')
                                @include('components.wizard.payment')
                                @include('components.wizard.shipped')
                            @elseif ($detail->transaksi->status_id == 4)
                                @include('components.wizard.placed')
                                @include('components.wizard.payment')
                                @include('components.wizard.shipped')
                                @include('components.wizard.status.complete')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 ">
        <div class="card">
            <div class="card-header">
                <h4>Detail Data {{$detail->transaksi->invoice_number}}</h4>
                <div class="card-header-action">
                    {{-- <a href="{{ route('order.create') }}" class="btn btn-primary">{{__('Tambah Data ')}} {{$title}} </a> --}}
                </div>
            </div>
            <div class="card-body">
                <table id="obat-table" class="table table-striped">
                    <thead>
                        <th>{{__('No')}}</th>
                        <th>{{__('Nama Obat')}}</th>
                        <th>{{__('Harga')}}</th>
                        <th>{{__('Kuantitas')}}</th>
                        <th>{{__('Tanggal Kadaluarsa')}}</th>
                    </thead>
                    <tbody>
                        <?php $no=1 ?>
                        @foreach ($data as $d)
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$d->obat->name}}</td>
                            {{-- <td></td> --}}
                            <td>{{$d->price}}</td>
                            <td>{{$d->quantity}}</td>
                            <td>{{$d->expired_date}}</td>
                            <?php $no++ ?>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- @php
                    dd($detail)
                @endphp --}}
            </div>

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>

</script>
@endpush