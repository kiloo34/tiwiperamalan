@extends('layouts.myview')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>{{ucfirst($title)}}</h4>
                <div class="card-header-action">
                    <a href="{{ route('ingoing.index') }}" class="btn btn-danger">Kembali </a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('ingoing.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>{{__('Obat')}}</label>
                        <select name="obat" id="obat" class="form-control @error('obat') is-invalid @enderror">
                            <option value=""></option>
                            @foreach ($obat as $o)
                                <option value="{{$o->id}}">{{$o->tipe->name}} {{$o->name}}</option>
                            @endforeach
                        </select>
                        @error('obat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('Jumlah')}}</label>
                        <div class="input-group">
                            <input name="jumlah" type="text" class="form-control @error('jumlah') is-invalid @enderror"
                                value="{{ old('jumlah') }}" autocomplete="jumlah" autofocus>
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <label for="type" id="type">Tipe Obat</label>
                                </div>
                            </div>
                            @error('jumlah')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{__('Expired Date')}}</label>
                        <input name="expired_date" type="text" id="exp-date" class="form-control @error('expired_date') is-invalid @enderror"
                            value="{{ old('expired_date') }}" autocomplete="expired_date" data-date-format='dd-mm-yyyy' autofocus>
                        @error('expired_date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <input type="submit" class="btn btn-primary float-right" value="Tambah">
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    var dateToday = new Date();
    $(document).ready(function(){
        $("#exp-date").datepicker({
            autoclose: true,
            startDate: dateToday,
            onSelect: function(selectedDate) {
                $('#exp-date').text();
            }
        });
        $("#obat").select2({
            placeholder: "Pilih Tipe Obat",
            allowClear: true
        });
        $('#obat').change(function() {
            var target = $(this).val();

            var url = '{{ route("ingoing.getType", ':obat') }}';
            url = url.replace(':obat', target);
            // console.log(url, target)
            $.get(url, function(data) {
                var select = $('#type');
                select.empty();
                $.each(data,function(key, value) {
                    select.append(value.name);
                });
            });
        });
    })
</script>
@endpush
@endsection

@include('import.select2')
@include('import.datepicker')
