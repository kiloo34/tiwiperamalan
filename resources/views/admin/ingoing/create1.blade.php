@extends('layouts.myview')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>{{__('Tambah Data Obat')}}</h4>
                <div class="card-header-action">
                    <a href="{{ route('ingoing.index') }}" class="btn btn-danger">Kembali </a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('dataObat.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>{{__('Obat')}}</label>
                        <select name="obat" id="obat" class="form-control @error('obat') is-invalid @enderror">
                            <option value=""></option>
                            @foreach ($obat as $o)
                                <option value="{{$o->id}}">{{$o->tipe->name}} {{$o->name}}</option>
                            @endforeach
                        </select>
                        @error('obat')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('Jumlah')}}</label>
                        <input name="jumlah" type="text" class="form-control @error('jumlah') is-invalid @enderror"
                                value="{{ old('jumlah') }}" autocomplete="jumlah" autofocus>
                        @error('jumlah')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label>{{__('Bulan')}}</label>
                                <input name="bulan" type="text" id="month" class="form-control @error('bulan') is-invalid @enderror"
                                    value="{{ old('bulan') }}" autocomplete="bulan" data-date-format='mm' autofocus>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label>{{__('Tahun')}}</label>
                                <input name="tahun" type="text" id="year" class="form-control @error('tahun') is-invalid @enderror"
                                    value="{{ old('tahun') }}" autocomplete="tahun" data-date-format='yyyy' autofocus>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary float-right" value="Tambah">
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    var dateToday = new Date();
    $(document).ready(function(){
        $("#exp-date").datepicker({
            autoclose: true,
            startDate: dateToday,
            onSelect: function(selectedDate) {
                $('#exp-date').text();
            }
        });
        $("#obat").select2({
            placeholder: "Pilih Tipe Obat",
            allowClear: true
        });
        $('#obat').change(function() {
            var target = $(this).val();

            var url = '{{ route("ingoing.getType", ':obat') }}';
            url = url.replace(':obat', target);
            $.get(url, function(data) {
                var select = $('#type');
                select.empty();
                $.each(data,function(key, value) {
                    select.append(value.name);
                });
            });
        });
        $("#month").datepicker({
            format: "mm",
            viewMode: "months", 
            minViewMode: "months",
            autoclose: true,
            onSelect: function(selectedDate) {
                $('#month').text();
            }
        });
        $("#year").datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years",
            autoclose: true,
            onSelect: function(selectedDate) {
                $('#year').text();
            }
        });
    })
</script>
@endpush
@endsection

@include('import.select2')
@include('import.datepicker')
