@extends('layouts.myview')
@section('content')
<div class="row">
    {{-- @include('components.infobox', ['col'=>'col-md-4 col-sm-6', 'icon'=>'user', 'isi'=>count($kecamatan),
    'judul'=>'kecamatan', 'link'=>route('kecamatan.index')])
    @include('components.infobox', ['col'=>'col-md-4 col-sm-6', 'icon'=>'clock', 'isi'=>count($periode),
    'judul'=>'periode', 'link'=>route('periode.index')]) --}}
</div>
<div class="row">
    <div class="col-md-6 col-md-12">
        <div class="card">
            <div class="card-header">
                <h4>Data Permintaan Obat</h4>
            </div>
            <div class="card-body">
                <form action="#" method="post" id="chart-peramalan">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 float-right">
                            <div class="form-group">
                                <label>{{__('Obat')}}</label>
                                <select name="obat" id="obat" class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label>{{__('Periode Awal ')}}</label>
                                <input type="text" name="awal" id="awal" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label>{{__('Periode Akhir')}}</label>
                                <input type="text" name="akhir" id="akhir" class="form-control">
                            </div>
                        </div>
                    </div>
                    <button type="submit" id="submit-btn" class="btn btn-primary float-right">{{__("Hitung")}}</button>
                </form>
                <canvas id="chartObat"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    var chartData = [];
    var chartLabel = [];
    
    const ctx1 = document.getElementById("chartObat").getContext('2d');    

    var data = {
        labels: [],
        datasets: []
    }

    const option = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: @json(ucfirst($title)),
            }
        },
        legend: {
            display: true
        },
        scales: {
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                    color: '#f2f2f2',
                },
                ticks: {
                    beginAtZero: true,
                    stepSize: 150
                }
            }],
            xAxes: [{
                ticks: {
                    display: false
                },
                gridLines: {
                    display: false
                }
            }]
        },
    }

    var config = {
        type: 'line',
        data: data,
        option: option
    }
    
    const myChart = new Chart(ctx1, config);   

    $(document).ready(function() {
        getObat();
        getTahun();
        $("#obat").select2();
        $("#awal").datepicker({
            autoclose: true,
            format: "mm-yyyy",
            viewMode: "months", 
            minViewMode: "months",
            onSelect: function(selectedDate) {
                $('#awal').text();
            }
        });
        $("#akhir").datepicker({
            autoclose: true,
            format: "mm-yyyy",
            viewMode: "months", 
            minViewMode: "months",
            onSelect: function(selectedDate) {
                $('#akhir').text();
            }
        });
    });
    
    function updateData(chart, label, data) {
        chart.data.labels = label;
        data.forEach(function (e,i) {
            chart.data.datasets[i] = e;
        })
        chart.update();
    }

    function getDataChart(obat, tahun) {
        console.log(obat, tahun);
        var url = '{{ route("admin.getDataChart", [':obat', ':tahun']) }}';
        url = url.replace(':obat', obat);
        url = url.replace(':tahun', tahun);
        $.ajax({
            type: "GET",
            url: url,
            success: function(res){
                console.log('YES')
                chartData = res.datasets
                chartLabel = res.label
                updateData(myChart, chartLabel, chartData)
            },
            error: function(xhr, textStatus){
                console.log('NO');
            }
        })
    }

    function getTahun() {
        var url = '{{ route("admin.getTahun") }}';
        $.get(url, function(data) {
            var select = $('#tahun');
            select.append('<option value="">Pilih Tahun</option>')
            $.each(data, function(key, value) {
                select.append('<option value=' + value + '>' + value + '</option>');
            });
            // select.select2("val", "");
        });
    }

    function getObat() {
        var url = '{{ route("admin.getObat") }}';
        $.get(url, function(data) {
            var select = $('#obat');
            select.append('<option value="null">Pilih Obat</option>')
            $.each(data, function(key, value) {
                select.append('<option value=' + value.id + '>' + value.name + '</option>');
            });
            // select.select2("val", "");
        });
    }

    function initChart() {
        // var tahun = $('#tahun').text()
        // var obat = $('#obat').val()

        // console.log(obat,tahun);
        // console.log(tahun.find(':selected').val(), obat.find(':selected').val());

        // console.log(obat, tahun);
        // console.log(obar);
        // var obat = $("#obat").find(":selected").val();
        // var tahun = $("#tahun").find(":selected").text();
        // console.log(obat, tahun);
        // getDataChart(obat, tahun);
    }

    $("#chart-peramalan").submit(function (e) { 
        e.preventDefault();

        var obat = $("#obat").find(":selected").val()
        var awal = $("#awal").datepicker('getDate')
        var akhir = $("#akhir").datepicker('getDate')

        if (obat == 'null' || awal == null || akhir == null) {
            checkInput(obat, awal, akhir)
            return false
        }

        var awalVal = awal.getMonth() +' '+awal.getYear()
        var akhirVal = akhir.getMonth() +' '+akhir.getYear()

        // var urlforecasting = '{{ route("admin.forecasting.calculateForecasting") }}';
        var urldata  = '{{ route("admin.getDataChart") }}';
        var data = {
            '__token': "{{ csrf_token() }}",
            'obat': obat,
            'awal': awal,
            'akhir': akhir,
        };
        getForecasting(urldata, data)
    });

    function getForecasting(url, data) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(data) {
                if (data.result != 0) {
                    // beta = data.result.length;
                    // dataTarget = data.result
                    // dataMape = data.mape
                    // dataShow = data.show_data
                    // console.log(dataTarget.length);
                    // for (let i = 0; i < beta; i++) {
                    //     showData(i, dataTarget[i]);
                    //     showMape(i, dataMape[i]);
                    // }
                    // setTimeout(function() {
                    //     showResult(dataShow)
                    // }, 10000);
                    console.log('YES')
                    // console.log(res.chart);
                    chartData = data.chart.datasets
                    chartLabel = data.chart.label
                    updateData(myChart, chartLabel, chartData)
                    console.log(chartLabel);
                    toastEvent('Hasil!', data.message, 'topRight', 'success')  
                    // $('#submit-btn').attr('disabled', true)
                    // $('#submit-btn').html('<span class="spinner-border spinner-border-sm" id="spinner-btn" role="status" aria-hidden="true"></span> Loading...')
                } else {
                    console.log(data);
                    toastEvent('Error!', data.message, 'topRight', 'error')   
                }
            }
        });   
    }
    
    function checkInput(obat,awal,akhir) { 
        var response = []
        var position = 'topRight'
        var title = 'Input Error'

        if (obat == 'null' && awal == null && akhir == null) {
            var text = 'input obat, Periode Awal, dan Periode Akhir kosong'
        } else if (obat == 'null') {
            var text = 'input obat kosong'
        } else if (awal == null) {
            var text = 'input Periode Awal kosong'
        } else if (akhir == null) {
            var text = 'input Periode Akhir kosong'
        }
        
        response.push(title)
        response.push(text)
        response.push(position)
        
        error_msg(response)
    }

    function error_msg(data) {
        iziToast.error({
            title: data[0],
            message: data[1],
            position: data[2],
        })
    }

    function toastEvent(title, message, position, type) {
            if (type == 'error') {
                iziToast.error({
                    title: title,
                    message: message,
                    position: position
                });   
            } else {
                iziToast.success({
                    title: title,
                    message: message,
                    position: position
                });
            }
        }
</script>
@endpush
@include('import.chartjs')
@include('import.select2')
@include('import.datepicker')