@extends('layouts.myview')
@section('content')
<div class="row">
    @include('components.infobox', ['col'=>'col-md-4 col-sm-6', 'icon'=>'list', 'isi'=>count($tipe),
    'judul'=>'Tipe', 'link'=>route('tipe.index')])
    {{-- @if (count($obat) == 0) --}}
    @include('components.infobox', ['col'=>'col-md-4 col-sm-6', 'icon'=>'clock', 'isi'=>count($obat),
    'judul'=>'Obat', 'link'=>count($obat) == 0 ? route('obat.create') : route('obat.index')])
    {{-- @endif --}}
</div>
<div class="row">
    <div class="col-md-12 ">
        <div class="card">
            <div class="card-header">
                <h4>Daftar {{ucwords($title)}}</h4>
                <div class="card-header-action">
                    <a href="{{ route('obat.create') }}" class="btn btn-primary">{{__('Tambah Data ')}} {{$title}} </a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="obat-table" class="table table-striped">
                        <thead>
                            <th>{{__('No')}}</th>
                            <th>{{__('Nama')}}</th>
                            <th>{{__('Tipe Obat')}}</th>
                            <th>{{__('Stok Obat')}}</th>
                            {{-- <th>{{__('Tanggal Expired')}}</th> --}}
                            <th>{{__('Aksi')}}</th>
                        </thead>
                        <tbody>
                            <?php $no=1 ?>
                            @foreach ($obat as $o)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$o->name}}</td>
                                <td>{{$o->tipe->name}}</td>
                                <td>{{$o->stock}}</td>
                                <td>
                                    <a href="{{ route('obat.show', $o->id) }}" class="btn btn-sm btn-icon icon-left btn-info"><i class="far fa-info"></i> {{__('detail')}}</a>
                                    <a href="{{ route('obat.edit', $o->id) }}" class="btn btn-sm btn-icon icon-left btn-primary"><i class="far fa-edit"></i> {{__('Ubah')}}</a>
                                    <a href="{{ route('obat.destroy', $o->id) }}"
                                        class="btn btn-sm btn-danger hapus-obat" data-toggle="tooltip" data-placement="top"
                                        title="Hapus Data" data-id="{{ $o->id }}">
                                        <i class="fa fa-trash"></i> {{__('Hapus')}}
                                    </a>
                                </td>
                                <?php $no++ ?>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#obat-table').DataTable();
    });

    $('.hapus-obat').on('click', function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var id = $(this).data("id");
        // var url = $('.hapus').attr('href');
        var url = "{{ route('obat.destroy', ":id") }}";
        url = url.replace(':id', id);
        $object=$(this);

        Swal.fire({
            title: 'Are you sure?',
            text: "Yakin ingin menghapus Data Obat ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    data: {id: id},
                    success: function (response) {
                        $($object).parents('tr').remove();
                        Swal.fire({
                            title: "Data Dihapus!",
                            text: response.message,
                            icon: 'success',
                        });
                        setTimeout(function () { 
                            location.reload();
                        }, 2500)
                    },
                    error: function (data) {
                        Swal.fire({
                            title: "Data Gagal Dihapus!",
                            icon: 'error',
                        })
                    }
                });
            }
        });
    })
</script>
@endpush
{{-- @include('import.chartjs') --}}
@include('import.datatable')
{{-- var ctx2 = document.getElementById("chartpermintaan").getContext('2d');
        var myChart = new Chart(ctx2, {
            type: 'bar',
            data: {
                labels: @json($label),
                datasets: [
                    @foreach ($chart as $c)
                    {
                        label: @json(ucfirst($c['kecamatan'])) ,
                        data: @json($c['permintaan']),
                        borderColor: '#'+Math.floor(Math.random()*16777215).toString(16),
                        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
                        pointRadius: 4
                    },
                    @endforeach
                ]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: @json(ucfirst($title)),
                    }
                },
                legend: {
                    display: true
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    x: {
                        stacked: true,
                    },
                    y: {
                        stacked: true
                    }
                },
            },
        }); --}}

        {{-- $(document).ready(function() {
            var ctx1 = document.getElementById("chartharga").getContext('2d');
            var myChart = new Chart(ctx1, {
                type: 'bar',
                data: {
                    labels: @json($label),
                    datasets: [
                        @foreach ($chart as $c)
                        {
                            label: @json(ucfirst($c['kecamatan'])) ,
                            data: @json($c['harga']),
                            borderColor: '#'+Math.floor(Math.random()*16777215).toString(16),
                            backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
                            pointRadius: 4
                        },
                        @endforeach
                    ]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: @json(ucfirst($title)),
                        }
                    },
                    legend: {
                        display: true
                    },
                    interaction: {
                        intersect: false,
                        mode: 'index',
                    },
                    scales: {
                        x: {
                            stacked: true,
                        },
                        y: {
                            stacked: true
                        }
                    },
                },
            });
    
    
        }); --}}