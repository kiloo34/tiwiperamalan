@extends('layouts.myview')
@section('content')
<div class="row">
    <div class="col-md-12 ">
        <div class="card">
            <div class="card-header">
                <h4>Detail Data {{ucwords($title)}}</h4>
                <div class="card-header-action">
                    <a href="{{ route('obat.index') }}" class="btn btn-primary">{{__('Kembali')}}</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="obat-table" class="table table-striped">
                        <thead>
                            <th>{{__('No')}}</th>
                            <th>{{__('Jumlah')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Tanggal Data')}}</th>
                            <th>{{__('Aksi')}}</th>
                        </thead>
                        <tbody>
                            <?php $no=1 ?>
                            @foreach ($data as $d)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$d->quantity}}</td>
                                <td>{{$d->created_at->toDateString()}}</td>
                                <td>{{$d->status == 0 ? 'Habis' : 'Tersedia'}}</td>
                                <td>
                                    @if ($d->status != 0)
                                        <a href="{{ route('admin.obat.tambahStock', $d->id) }}" class="btn btn-sm btn-icon icon-left btn-success">
                                            <i class="far fa-plus"></i> 
                                            {{__('Tambah ke Stock obat')}}
                                        </a>
                                    @endif
                                    {{-- <a href="{{ route('obat.edit', $d->id) }}" class="btn btn-sm btn-icon icon-left btn-primary"><i class="far fa-edit"></i> {{__('Ubah')}}</a> --}}
                                    {{-- <a href="{{ route('obat.destroy', $d->id) }}"
                                        class="btn btn-sm btn-danger hapus-obat" data-toggle="tooltip" data-placement="top"
                                        title="Hapus Data" data-id="{{ $d->id }}">
                                        <i class="fa fa-trash"></i> {{__('Hapus')}}
                                    </a> --}}
                                </td>
                                <?php $no++ ?>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#obat-table').DataTable();
    });

    // $('.hapus-obat').on('click', function (e) {
    //     e.preventDefault();

    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });

    //     var id = $(this).data("id");
    //     // var url = $('.hapus').attr('href');
    //     var url = "{{ route('obat.destroy', ":id") }}";
    //     url = url.replace(':id', id);
    //     $object=$(this);

    //     Swal.fire({
    //         title: 'Are you sure?',
    //         text: "Yakin ingin menghapus Data Obat ini!",
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Ya!'
    //     }).then((result) => {
    //         if (result.value) {
    //             $.ajax({
    //                 url: url,
    //                 type: 'DELETE',
    //                 data: {id: id},
    //                 success: function (response) {
    //                     $($object).parents('tr').remove();
    //                     Swal.fire({
    //                         title: "Data Dihapus!",
    //                         text: response.message,
    //                         icon: 'success',
    //                     });
    //                     setTimeout(function () { 
    //                         location.reload();
    //                     }, 2500)
    //                 },
    //                 error: function (data) {
    //                     Swal.fire({
    //                         title: "Data Gagal Dihapus!",
    //                         icon: 'error',
    //                     })
    //                 }
    //             });
    //         }
    //     });
    // })
</script>
@endpush
@include('import.datatable')