@extends('layouts.myview')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>{{ucfirst($title)}}</h4>
                <div class="card-header-action">
                    <a href="{{ route('obat.index') }}" class="btn btn-danger">Kembali </a>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('obat.update', $obat->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <div class="form-group">
                        <label>{{__('Tipe Obat')}}</label>
                        <select name="tipe" id="tipe" class="form-control @error('tipe') is-invalid @enderror">
                            <option value="{{$obat->tipe->id}}">{{$obat->tipe->name}}</option>
                            @foreach ($tipe as $t)
                                <option value="{{$t->id}}">{{$t->name}}</option>
                            @endforeach
                        </select>
                        @error('tipe')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('Nama')}}</label>
                        <input name="name" type="text" class="form-control @error('name') is-invalid @enderror"
                            value="{{$obat->name}}" autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('Satuan')}}</label>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 mt-2">
                                <div class="input-group">
                                    <input name="mg" type="text" class="form-control @error('mg') is-invalid @enderror"
                                        value="{{ old('mg') }}" autocomplete="mg" autofocus>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            mg
                                        </div>
                                    </div>
                                </div>
                                @error('mg')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-md-6 col-sm-12 mt-2">
                                <div class="input-group">
                                    <input name="ml" type="text" class="form-control @error('ml') is-invalid @enderror"
                                        value="{{ old('ml') }}" autocomplete="ml" autofocus>
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            ml
                                        </div>
                                    </div>
                                </div>
                                @error('ml')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>    
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{__('Expired Date')}}</label>
                        <input name="expired_date" type="text" id="exp-date" class="form-control @error('expired_date') is-invalid @enderror"
                            value="{{$obat->expired_date}}" autocomplete="expired_date" data-date-format='dd-mm-yyyy' autofocus>
                        @error('expired_date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <input type="submit" class="btn btn-primary float-right" value="Tambah">
                </form>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    var dateToday = new Date();

    $(document).ready(function(){
        $("#exp-date").datepicker({
            autoclose: true,
            // minViewMode: 1,
            // language: 'id',
            // format: 'MM',
            startDate: dateToday,
            onSelect: function(selectedDate) {
                $('#exp-date').text();
            }
        });
        $("#tipe").select2({
            placeholder: "Pilih Tipe Obat",
            allowClear: true
        });
    })
</script>
@endpush
@endsection

@include('import.select2')
@include('import.datepicker')
