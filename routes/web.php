<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\DashboardController as adminDashboard;
use App\Http\Controllers\Admin\MedicineController as adminMed;
use App\Http\Controllers\Admin\MedicineTypeController as adminTypeMed;
use App\Http\Controllers\Admin\RecapController as adminRecapMed;
use App\Http\Controllers\Admin\IngoingController as adminObatMasuk;
use App\Http\Controllers\Admin\MedicineDataController as adminDataObat;
use App\Http\Controllers\Admin\OutgoingController as adminObatKeluar;
use App\Http\Controllers\Admin\ForecastingController as adminPeramalan;

use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::middleware(['auth'])->group(function () {
    Route::group(['middleware' => ['role:admin']], function () {
        //Dashboard
        Route::get('/dashboard', [adminDashboard::class, 'index'])->name('admin.dashboard');
        Route::post('ajax/getDataChart', [adminDashboard::class, 'getDataChart'])->name('admin.getDataChart');
        Route::get('ajax/getDataTahun', [adminDashboard::class, 'getDataTahun'])->name('admin.getTahun');
        Route::get('ajax/getDataObat', [adminDashboard::class, 'getDataObat'])->name('admin.getObat');
        Route::get('ajax/getDataLabel', [adminDashboard::class, 'getDataLabel'])->name('admin.getLabel');
        // Obat
        Route::resource('obat', adminMed::class);
        Route::get('obat/tambahStock/{id}', [adminMed::class, 'tambahStock'])->name('admin.obat.tambahStock');
        // Type
        Route::resource('tipe', adminTypeMed::class);
        // Recap
        Route::resource('rekap', adminRecapMed::class);
        Route::get('ajax/rekap/getTransaction', [adminRecapMed::class, 'getData'])->name('admin.rekap.getData');
        // Permintaan Transaction
        Route::resource('ingoing', adminObatMasuk::class);
        Route::resource('dataObat', adminDataObat::class);
        Route::get('ingoing/{id}/tipe', [adminObatMasuk::class, 'getType'])->name('ingoing.getType');
        // Obat Keluar Transaction
        Route::resource('outgoing', adminObatKeluar::class);
        Route::get('outgoing/{id}/tipe', [adminObatKeluar::class, 'getType'])->name('outgoing.getType');
        Route::get('ajax/outgoing/obat_all', [adminObatKeluar::class, 'getObat'])->name('outgoing.getObat');
        Route::get('ajax/outgoing/keranjang_all', [adminObatKeluar::class, 'getKeranjang'])->name('outgoing.getKeranjang');
        Route::get('ajax/outgoing/detail/{id}', [adminObatKeluar::class, 'getDetailTransaction'])->name('outgoing.getDetailInfo');
        Route::post('ajax/outgoing/{transaction}/obat/{medicine}/addCart', [adminObatKeluar::class, 'addCart'])->name('outgoing.addCart');
        Route::post('ajax/outgoing/{transaction}/obat/{medicine}/deleteItem', [adminObatKeluar::class, 'deleteItem'])->name('outgoing.deleteCartItem');
        Route::post('ajax/outgoing/{transaction}/obat/{medicine}/updateItem', [adminObatKeluar::class, 'updateItem'])->name('outgoing.updateCartItem');
        // Forecasting
        Route::get('/forecasting', [adminPeramalan::class, 'index'])->name('admin.forecasting');
        Route::get('ajax/forecasting/getDataTahun/', [adminPeramalan::class, 'getDataTahun'])->name('admin.forecasting.getDataTahun');
        Route::get('ajax/forecasting/getDataBulan/', [adminPeramalan::class, 'getDataBulan'])->name('admin.forecasting.getDataBulan');
        Route::get('ajax/forecasting/getDataObat/', [adminPeramalan::class, 'getDataObat'])->name('admin.forecasting.getDataObat');
        Route::post('ajax/forecasting/calcultaeForecasting/', [adminPeramalan::class, 'calcultaeForecasting'])->name('admin.forecasting.calculateForecasting');
    });
});

Route::get('/landing', [HomeController::class, 'index'])->name('landing');

