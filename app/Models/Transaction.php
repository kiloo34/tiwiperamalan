<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'transactions';
    protected $primaryKey = 'id';

    protected $fillable = [
        'invoice_number',
        'status_id',
        'type_id',
        'created_by',
        'updated_by'
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    protected $with = ['detail_transaksi'];

    /**
     * Get the status that owns the Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status_transaksi()
    {
        return $this->belongsTo(TransactionStatus::class, 'status_id');
    }

    /**
     * Get all of the order_detail for the Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detail_transaksi()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id');
    }
}
