<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;

    protected $table = 'transaction_details';
    protected $primaryKey = 'id';

    protected $fillable = [
        'transaction_id',
        'medicine_id',
        'price',
        'quantity',
        'created_at',
    ];

    protected $with = ['obat'];
    protected $dates = ['expired_date', 'updated_at'];

    /**
     * Get the order that owns the OrderDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaksi()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }

    /**
     * Get the obat that owns the OrderDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function obat()
    {
        return $this->belongsTo(Medicine::class, 'medicine_id');
    }
}
