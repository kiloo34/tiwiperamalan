<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
    use HasFactory;

    protected $table = 'medicines';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'weight',
        'stock',
        'type_id'
    ];

    // protected $dates = ['expired_date'];
    protected $with = ['tipe'];

    /**
     * Get all of the tipe for the Medicine
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipe()
    {
        return $this->belongsTo(MedicineType::class, 'type_id');
    }

    /**
     * Get all of the detail for the Medicine
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detail()
    {
        return $this->hasMany(MedicineDetail::class, 'medicine_id');
    }

    /**
     * Get all of the order_detail for the Medicine
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_detail()
    {
        return $this->hasMany(TransactionDetail::class, 'medicine_id');
    }


}
