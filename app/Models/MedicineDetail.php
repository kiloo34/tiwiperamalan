<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicineDetail extends Model
{
    use HasFactory;

    protected $table = 'medicine_details';
    protected $primaryKey = 'id';

    protected $fillable = [
        'medicine_id',
        'quantity'
    ];

    protected $dates = ['expired_date', 'created_at', 'updated_at'];

    // protected $dates = ['expired_date'];

    /**
     * Get the obat that owns the MedicineDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function obat()
    {
        return $this->belongsTo(Medicine::class, 'medicine_id');
    }
}
