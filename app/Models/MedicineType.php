<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicineType extends Model
{
    use HasFactory;
    
    protected $table = 'medicine_types';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name'
    ];

    // protected $with = ['obat'];

    public function obat()
    {
        return $this->hasOne(Medicine::class, 'type_id');
    }
}
