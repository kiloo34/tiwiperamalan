<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{
    protected $table = 'transaction_statuses';
    // protected $primaryKey = 'id';

    protected $fillable = [
        'name'
    ];

    /**
     * Get the order associated with the OrderStatus
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transaksi()
    {
        return $this->hasOne(Transaction::class);
    }

    /**
     * Get all of the obat for the OrderStatus
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function obat()
    {
        return $this->hasMany(Medicine::class, 'medicine_id');
    }
}
