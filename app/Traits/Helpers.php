<?php

namespace App\Traits;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DateInterval;
use DatePeriod;
use DateTime;

trait Helpers {

    protected function minYear($data)
    {
        return Carbon::create($data)->format('Y');
    }

    protected function maxYear($data)
    {
        return Carbon::create($data)->format('Y');
    }

    protected function startMonth($data)
    {
        return new DateTime($data);
    }

    protected function endMonth($data)
    {
        return new DateTime($data);
    }

    // forecasting 0.1
    // protected function getYears($min,$max)
    // {
    //     for ($j=0; $j <= $max-$min; $j++) { 
    //         $tahun[] = $min+$j;
    //     }
    //     return $tahun;
    // }

    // forecasting 0.2
    protected function getOneYears($min,$max)
    {
        $new_min = $min;
        $new_max = $max;
        for ($j=0; $j <= $new_max-$new_min; $j++) { 
            $tahun[] = $new_min+$j;
        }
        return $tahun;
    }

    protected function getYears($min,$max)
    {
        $new_min = ($min+4);
        $new_max = ($max+4);
        for ($j=0; $j <= $new_max-$new_min; $j++) { 
            $tahun[] = $new_min+$j;
        }
        return $tahun;
    }

    protected function getMonthsRange($month, $year)
    {
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod(
                        // $this->startMonth($month . ' ' . ($year - 1)), // forecasting 0.1 
                        $this->startMonth($month . ' ' . ($year - 4)), 
                        $interval, 
                        $this->endMonth($month . ' ' . $year)
                    );

        $data = array();

        foreach ($period as $dt) {
            $data[] = $dt->format("F Y");
        }

        return $data;
    }

    protected function getMonths()
    {
        $bulan = array();
        for ($i=0; $i < Carbon::MONTHS_PER_YEAR; $i++) { 
            $month = Carbon::create()->month($i+1);
            $bulan[] = $month->format('F'); 
        }
        return $bulan;
    }

    protected function countMultipleStockData($data)
    {
        if (count($data) > 1) {
            $res = 0;
            foreach ($data as $d) {
                $res += $d->quantity;
            }
        } else {
            $res = (int)$data->first()->quantity;
        }
        return $res;
    }

}