<?php

namespace App\Traits;

trait Arsess {

    protected function countF($alpha, $X, $F) {
        return round(($alpha * $X)+(( 1-$alpha )* $F), 2);
    }

    protected function countESmal($X,$F) {
        return round($X-$F, 2);
    }

    protected function countEBig($e,$beta,$E){
        return round(($beta*$e)+((1-$beta)*$E), 2);
    }

    protected function countAlfa($E,$EA){
        return round(abs( $E/$EA ), 2);
    }

    protected function countPE($X,$F){
        return round(abs((( $X-$F )/$X ) * 100), 2);
    } 

    protected function countEA($e, $beta, $EA)
    {
        return round(($beta * abs($e)) + (( 1-$beta ) * $EA), 2);
    }

    protected function getBeta(){
        return array(0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9);
    }

    protected function countMAPE($data)
    {
        // return array_sum($data['PE']) / (count($data) - 1);
        $total = 0;
        foreach ($data as $d) {
            $total += $d['PE'];
        }
        return round($total / (count($data) - 1), 2);
    }
}