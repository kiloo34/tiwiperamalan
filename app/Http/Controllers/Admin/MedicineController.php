<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Medicine;
use App\Models\MedicineDetail;
use App\Models\MedicineType;
use DateTime;
use Illuminate\Http\Request;

class MedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = MedicineType::all();
        $obat = Medicine::with(['tipe'])->get();
        return view('admin.medicine.index', [
            'title' => 'obat',
            'subtitle' => '',
            'active' => 'obat',
            'tipe' => $type,
            'obat' => $obat
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = MedicineType::all();

        if (count($type) == 0) {
            return redirect()->route('obat.index')->with('error_msg', 'Silakan menambahkan Tipe obat sebelum menambahkan obat');
        }

        return view('admin.medicine.create', [
            'title' => 'obat',
            'subtitle' => 'create',
            'active' => 'obat',
            'tipe' => $type
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $target = Medicine::where([
            ['name', $request->name],
            ['type_id', $request->tipe]
        ])->with('tipe')->first();

        if ($target) {
            return redirect()->back()->with('error_msg', 'Data Obat ' . $request->name . ' dengan tipe ' . $target->tipe->name . ' Sudah tersedia');
        }

        $request->validate([
            'name' => 'required|regex:/^[a-zA-Z ]+$/',
            'tipe' => 'required',
            'mg' => 'nullable|numeric',
            'ml' => 'nullable|numeric',
            // 'expired_date' => 'required|date'
        ], [
            // 'name.unique' => 'Obat sudah ditambahkan',
            'name.regex' => 'Obat harus huruf',
            'name.required' => 'Obat harap diisi',
            'tipe.required' => 'Tipe harus diisi',
            'mg.numeric' => 'satuan miligram harus angka',
            'ml.numeric' => 'satuan mililiter harus angka',
        ]);

        if ($request->mg != null && $request->ml == null) {
            $weight = $request->mg;
        } elseif ($request->mg == null && $request->ml != null) {
            $weight = $request->ml;
        } else {
            $weight = $request->mg.'/'.$request->ml;
        }

        $date = new DateTime($request->expired_date);
        // dd();

        $data = Medicine::create([
            'name' => ucfirst($request->name),
            'type_id' => $request->tipe,
            'weight' => $weight, 
            // 'expired_date' => $date->format('Y-m-d H:i:s')
        ]);

        return redirect()->route('obat.index')->with('success_msg', 'Obat ' . $data->name . ' ' . $weight .' berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $data = Medicine::findOrFail($id);
        $data = MedicineDetail::where('medicine_id', $id)
                    ->orderBy('created_at', 'desc')
                    ->get();
        // dd($data);
        return view('admin.medicine.show', [
            'title' => 'obat',
            'subtitle' => 'show',
            'active' => 'obat',
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obat = Medicine::findOrFail($id);
        $type = MedicineType::all();
        return view('admin.medicine.edit', [
            'title' => 'tipe',
            'subtitle' => 'edit',
            'active' => 'obat',
            'tipe' => $type,
            'obat' => $obat
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|regex:/^[a-zA-Z ]+$/|unique:medicines,name',
            'tipe' => 'required',
            'mg' => 'nullable|numeric',
            'ml' => 'nullable|numeric',
            // 'expired_date' => 'required|date'
        ], [
            'name.unique' => 'Obat sudah ditambahkan',
            'name.regex' => 'Obat harus huruf',
            'name.required' => 'Obat harap diisi',
            'tipe.required' => 'Tipe harus diisi',
            'mg.numeric' => 'satuan miligram harus angka',
            'ml.numeric' => 'satuan mililiter harus angka',
            // 'expired_date.required' => 'tanggal expired harus diisi',
            // 'expired_date.date' => 'tanggal expired harus format tanggal',
        ]);

        if ($request->mg != null && $request->ml == null) {
            $weight = $request->mg;
        } elseif ($request->mg == null && $request->ml != null) {
            $weight = $request->ml;
        } elseif ($request->mg != null && $request->ml != null) {
            $weight = $request->mg.'/'.$request->ml;
        } else {
            $weight = null;
        }

        $date = new DateTime($request->expired_date);

        Medicine::where('id', $id)
            ->update([
                'name' => ucfirst($request->name),
                'type_id' => $request->tipe,
                'weight' => $weight, 
                // 'expired_date' => $date->format('Y-m-d H:i:s')
            ]);

        return redirect()->route('obat.index')->with('success_msg', 'Obat ' . $request->name . ' berhasil diperbaruhi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $target = Medicine::where('id', $id);
        $target->delete();
        return response()->json([
            'message' => 'Obat berhasil dihapus!'
        ]);
    }

    public function tambahStock($id)
    {
        $data = MedicineDetail::where('id', $id)->first();
        // dd('masuk', $data);
        $data->update([
            'status' => 0
        ]);
        $obat = Medicine::where('id', $data->medicine_id)->first();
        $obat->update([
            'stock' => $obat->stock + $data->quantity
        ]);
        return redirect()->route('obat.index')->with('success_msg', 'Stock Obat ' . $obat->name . ' berhasil ditambah');
    }
}
