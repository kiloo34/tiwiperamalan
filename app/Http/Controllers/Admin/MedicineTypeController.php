<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MedicineType;
use Illuminate\Http\Request;

class MedicineTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = MedicineType::all();
        return view('admin.medicine_type.index', [
            'title' => 'tipe obat',
            'subtitle' => '',
            'active' => 'obat',
            'tipe' => $type
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.medicine_type.create', [
            'title' => 'tipe',
            'subtitle' => 'create',
            'active' => 'obat',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $target = MedicineType::where([
            ['name', $request->name],
        ])->first();

        if ($target) {
            return redirect()->back()->with('error_msg', 'Data Tipe Obat ' . $request->name . ' Sudah tersedia');
        }

        $request->validate([
            'name' => 'required|regex:/^[a-zA-Z ]+$/|unique:medicine_types,name'
        ], [
            'name.unique' => 'Tipe Obat sudah ditambahkan',
            'name.regex' => 'Tipe Obat harus huruf',
            'name.required' => 'Tipe Obat harap diisi'
        ]);
        
        $data = MedicineType::create([
            'name' => ucfirst($request->name)
        ]);

        return redirect()->route('obat.index')->with('success_msg', 'Tipe Obat ' . $data->name . ' berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MedicineType::findOrFail($id);
        return view('admin.medicine_type.show', [
            'title' => 'tipe obat',
            'subtitle' => 'show',
            'active' => 'obat',
            'tipe' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MedicineType::findOrFail($id);
        return view('admin.medicine_type.edit', [
            'title' => 'tipe',
            'subtitle' => 'edit',
            'active' => 'obat',
            'tipe' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|regex:/^[a-zA-Z ]+$/|unique:medicine_types,name,' . $id
        ], [
            'name.unique' => 'Tipe Obat '.$request->name.' sudah ditambahkan',
            'name.regex' => 'Tipe Obat harus huruf',
            'name.required' => 'Tipe Obat harap diisi'
        ]);

        MedicineType::where('id', $id)
            ->update(['name' => $request->name]);

        return redirect()->route('tipe.index')->with('success_msg', 'Tipe Obat ' . $request->name . ' berhasil diperbaruhi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $target = MedicineType::where('id', $id);
        $target->delete();
        return response()->json([
            'message' => 'Tipe Obat berhasil dihapus!'
        ]);
    }
}
