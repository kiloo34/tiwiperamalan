<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Medicine;
use App\Models\MedicineDetail;
use App\Models\MedicineType;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\TransactionStatus;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IngoingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permintaan = Transaction::where('deleted_at', null)
            ->whereNotIn('status_id', [5])
            ->whereIn('type_id', [1])
            ->with(['status_transaksi','detail_transaksi'])->get();
            
        return view('admin.ingoing.index', [
            'title' => 'ingoing',
            'subtitle' => '',
            'active' => 'permintaan',
            'order' => $permintaan,
            // 'obat' => $obat
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $obat = Medicine::all();
        return view('admin.ingoing.create', [
            'title' => 'ingoing',
            'subtitle' => 'create',
            'active' => 'permintaan',
            'obat' => $obat
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'obat' => 'required',
            'jumlah' => 'required|numeric',
            'expired_date' => 'required|date'
        ], [
            'obat.required' => 'Obat harap diisi',
            'jumlah.required' => 'Tipe harus diisi',
            'jumlah.numeric' => 'satuan miligram harus angka',
            'expired_date.required' => 'tanggal expired harus diisi',
            'expired_date.date' => 'tanggal expired harus format tanggal',
        ]);

        $date = new DateTime($request->expired_date);
        $invoice = $this->generateInvoice(); 
        $user = Auth::user();
        $order = Transaction::create([
            'invoice_number' => $invoice,
            'status_id' => 2,
            'type_id' => 1,
            'created_by' => $user->id
        ]);

        TransactionDetail::insert([
            'transaction_id' => $order->id,
            'medicine_id' => $request->obat,
            'price' => '0',
            'quantity' => $request->jumlah,
            'expired_date' => $date->format('Y-m-d H:i:s'),
        ]);

        return redirect()->route('ingoing.index')->with('success_msg', 'Tambah Obat Masuk berhasil ditambah dengan status dikirim');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = TransactionDetail::where('transaction_id', $id)->with('transaksi', 'obat')->first();
        $data = TransactionDetail::where('transaction_id', $id)->with('transaksi', 'obat')->get();
        // dd($data);

        return view('admin.ingoing.show', [
            'title' => 'ingoing',
            'subtitle' => 'show',
            'active' => 'permintaan',
            'detail' => $detail,
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pesan = '';
        $data = Transaction::findOrFail($id);
        $coba  = '';
        if ($data != null) {
            if ($data->status_id == 1) {
                Transaction::where('id', $id)
                    ->update([
                        'status_id' => 2
                    ]);
                $pesan = 'Status Data Order berhasil diupdate';
            } elseif ($data->status_id == 2) {
                Transaction::where('id', $id)
                    ->update([
                        'status_id' => 3
                    ]);
                $pesan = 'Status Data Order berhasil diupdate';
            }  elseif ($data->status_id == 3) {
                Transaction::where('id', $id)
                    ->update([
                        'status_id' => 4
                    ]);
                $coba = MedicineDetail::insert([
                    'medicine_id' => $data->detail_transaksi[0]->medicine_id,
                    'quantity' => $data->detail_transaksi[0]->quantity,
                    'expired_date' => $data->detail_transaksi[0]->expired_date,
                    'status' => '1',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
                $pesan = 'Status Data Order berhasil diupdate';
            }
            $coba = $coba;
        } else {
            $pesan = 'Data Order tidak ditemukan';
        }
        
        return response()->json([
            'message' => $pesan,
            'coba' => $coba
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = TransactionStatus::where('name', 'batal')->first();
        Transaction::where('id', $id)
            ->update([
                'status_id' => $status->id
            ]);
        $target = Transaction::where('id', $id);
        $target->delete();
        return response()->json([
            'message' => 'Data Order berhasil dihapus!'
        ]);
    }

    public function getType($id)
    {
        return MedicineType::where('id', $id)->get();
    }

    private function generateInvoice()
    {
        $date   = new DateTime();

        $result = $date->format('Y-m-d-H-i-s');
        $krr    = explode('-', $result);
        $finv   = implode("", $krr);
        
        $digits = 3;
        $ra = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $res = 'INV'.$ra.$finv;

        return $res;
    }
}
