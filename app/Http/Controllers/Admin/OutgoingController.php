<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Medicine;
use App\Models\MedicineType;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\TransactionStatus;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class OutgoingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outgoing = Transaction::where('deleted_at', null)
            ->whereNotIn('status_id', [5])
            ->whereIn('type_id', [2])
            ->with(['status_transaksi','detail_transaksi'])->get();
        // dd($outgoing);
            
        return view('admin.outgoing.detail_index', [
            'title' => 'outgoing',
            'subtitle' => '',
            'active' => 'outgoing',
            'order' => $outgoing,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (count($this->listTransaction()) >= 1) {
            return redirect()->route('outgoing.index')->with('error_msg', 'ada transaksi yang belum selesai');
        } else {
            $user = Auth::user();
            $order = Transaction::create([
                'invoice_number' => '-',
                'status_id' => 6,
                'type_id' => 2,
                'created_by' => $user->id
            ]);
            return redirect()->route('outgoing.show', $order->id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Transaction::findOrFail($id);
        $detail = TransactionDetail::where('transaction_id', $data->id)->get();
        $obat = Medicine::all();
        // dd($data->id);
        return view('admin.outgoing.detail_create', [
            'title' => 'outgoing',
            'subtitle' => 'show',
            'active' => 'outgoing',
            'data' => $data,
            'detail' => $detail,
            'obat' => $obat,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = '';
        if ($request->ajax()) {
            $data = Transaction::findOrFail($id);
            $link = '';
            if ($data) {
                $user = Auth::user();
                
                $this->updateTransaction($user->id, $data->id);
                $data->status_id = 4;
                $data->invoice_number = $this->generateInvoice();
                $data->save();
                // $link = '{{ route("outgoing.update") }}';
                $message = 'Data Order berhasil di input';
            } else {
                $message = 'Data Order tidak ditemukan';
            }

            return response()->json([
                'message'   => $message,
                'link'      => $link
            ]);
        } else {
            $message = 'only ajax request';
            return response()->json([
                'message' => $message,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = TransactionStatus::where('name', 'batal')->first();
        Transaction::where('id', $id)
            ->update([
                'status_id' => $status->id
            ]);
        $target = Transaction::where('id', $id);
        $target->delete();
        return response()->json([
            'message' => 'Data Order berhasil dihapus!'
        ]);
    }

    public function getObat(Request $request)
    {   
        if ($request->ajax()) {
            $data = Medicine::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('name', function($row){
                    $obat = $row->name . ' ' . $row->tipe->name;
                    return $obat;
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" class="btn btn-success btn-sm btn-icon" onclick="addDetail('.$row->id.')">
                                    <i class="fas fa-plus"></i>
                                </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function getKeranjang(Request $request)
    {
        if ($request->ajax()) {
            $data = TransactionDetail::whereHas('transaksi', function($query){
                        $query->where('deleted_at', null)
                            ->where('invoice_number', '-')
                            ->whereIn('status_id', [6])
                            ->whereIn('type_id', [2]);
                    })
                    ->get();
                    
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('name', function($row){
                    $obat = $row->obat->name . ' ' . $row->obat->tipe->name;
                    return $obat;
                })
                ->addColumn('stock', function($row){
                    $stok = $row->quantity;
                    return $stok;
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" class="btn btn-danger btn-sm btn-icon" onclick="updateItem('.$row->obat->id.')">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-sm btn-icon" onclick="removeItem('.$row->obat->id.')">
                                    <i class="fas fa-trash"></i>
                                </button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function getDetailTransaction(Request $request, $id)
    {
        if ($request->ajax()) {
            $data = TransactionDetail::where('transaction_id', $id)
                    ->get();
                    
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('name', function($row){
                    $obat = $row->obat->name . ' ' . $row->obat->tipe->name;
                    return $obat;
                })
                ->addColumn('jumlah', function($row){
                    $stok = $row->quantity;
                    return $stok;
                })
                ->make(true);
        }
    }

    public function addCart(Request $request, Transaction $transaction, Medicine $medicine)
    {
        $message = '';
        $code = '';
        if ($request->ajax()) {
            $user = Auth::user();
            if ($this->cekMedicineStock($medicine->id)->stock != 0) {
                $this->updateTransaction($user->id, $transaction->id);
                if ($this->cekMedicineTransactionDetail($transaction->id, $medicine->id) != null) {
                    $this->updateStockDetailPlus($transaction->id, $medicine->id);
                    $this->updateStockMedicineMinus($medicine->id);
                    $message = 'Kuantitas Berhasil Ditambahkan';
                } else {
                    $detail = TransactionDetail::create([
                        'transaction_id'    => $transaction->id,
                        'medicine_id'       => $medicine->id,
                        'price'             => 0,
                        'quantity'          => 1
                    ]);
                    $this->updateStockMedicineMinus($medicine->id);
                    $message = 'Data Obat Berhasil Ditambahkan';
                }
            } else {
                $message = 'Stock Obat tidak Tersedia';
            }
            return response()->json([
                // 'obat' => $medicine,
                // 'transaksi' => $transaction,
                'message'   => $message,
                'code'      => $code
            ]);
        } else {
            $message = 'only ajax request';
            return response()->json([
                'message' => $message,
            ]);
        }
    }

    public function updateItem(Request $request, Transaction $transaction, Medicine $medicine)
    {
        $message = '';
        if ($request->ajax()) {
            $user = Auth::user();

            $this->updateTransaction($user->id, $transaction->id);
            $this->updateStockDetailMinus($transaction->id, $medicine->id);
            $this->updateStockMedicinePlus($medicine->id);

            $cek = $this->cekMedicineTransactionDetail($transaction->id, $medicine->id);

            if ($cek->quantity == 0) {
                $cek->delete();
                $message = 'Item Berhasil Dihapus';
            } else {
                $message = 'Kuantitas Berhasil Dikurangi';
            }

            return response()->json([
                'obat' => $medicine,
                'transaksi' => $transaction,
                'message' => $message,
                'cek'=>$cek
            ]);
        } else {
            $message = 'only ajax request';
            return response()->json([
                'message' => $message,
            ]);
        }
    }

    public function deleteItem(Request $request, Transaction $transaction, Medicine $medicine)
    {
        $message = '';
        if ($request->ajax()) {
            $cek = $this->cekMedicineTransactionDetail($transaction->id, $medicine->id);
            $this->updateStockMedicineRestore($medicine->id,$cek->quantity);
            $cek->delete();
            $message = 'Item Berhasil Dihapus';

            return response()->json([
                'obat' => $medicine,
                'transaksi' => $transaction,
                'message' => $message,
                'cek' => $cek
            ]);
        } else {
            $message = 'only ajax request';
            return response()->json([
                'message' => $message,
            ]);
        }
    }

    protected function cekMedicineTransactionDetail($transaction_id, $medicine_id)
    {
        return TransactionDetail::where('transaction_id', $transaction_id)->where('medicine_id', $medicine_id)->first();
    }

    protected function cekMedicineStock($medicine_id)
    {
        return Medicine::findOrFail($medicine_id);
    }

    protected function updateTransaction($user_id, $transaction_id)
    {
        $transaksi = Transaction::findOrFail($transaction_id);
        $transaksi->updated_by = $user_id;
        $transaksi->save();
    }
    
    protected function updateStockDetailPlus($transaction_id, $medicine_id)
    {
        $detail = $this->cekMedicineTransactionDetail($transaction_id, $medicine_id);
        $detail->quantity = $detail->quantity + 1;
        $detail->save();
    }

    protected function updateStockDetailMinus($transaction_id, $medicine_id)
    {
        $detail = $this->cekMedicineTransactionDetail($transaction_id, $medicine_id);
        $detail->quantity = $detail->quantity - 1;
        $detail->save();
    }

    protected function updateStockMedicinePlus($medicine_id)
    {
        $obat = Medicine::findOrFail($medicine_id);
        $obat->stock = $obat->stock + 1;
        $obat->save();
    }

    protected function updateStockMedicineMinus($medicine_id)
    {
        $obat = Medicine::findOrFail($medicine_id);
        $obat->stock = $obat->stock - 1;
        $obat->save();
    }

    protected function updateStockMedicineRestore($medicine_id, $total)
    {
        $obat = Medicine::findOrFail($medicine_id);
        $obat->stock = $obat->stock + $total;
        $obat->save();
    }

    protected function listTransaction()
    {
        return Transaction::where('deleted_at', null)
            ->where('invoice_number', '-')
            ->whereIn('status_id', [6])
            ->whereIn('type_id', [2])
            ->get();
    }

    private function generateInvoice()
    {
        $date   = new DateTime();

        $result = $date->format('Y-m-d-H-i-s');
        $krr    = explode('-', $result);
        $finv   = implode("", $krr);
        
        $digits = 3;
        $ra = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $res = 'INV'.$ra.$finv;

        return $res;
    }
}
