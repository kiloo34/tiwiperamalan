<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Medicine;
use App\Models\MedicineDetail;
use App\Traits\Arsess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use App\Traits\Mape;
use Faker\Extension\Helper;

class DashboardController extends Controller
{
    use Helpers, Arsess, Mape;

    public function index()
    {
        return view('admin.dashboard', [
            'title' => 'dashboard',
            'subtitle' => '',
            'active' => 'dashboard',
        ]);
    }

    public function getDataChart(Request $request)
    {
        $chart = [];
        $x = [];
        $message = '';
        $res = [];
        $mape = [];
        // $show_data = [];
        
        $carbon = new Carbon();
        $periode_awal = preg_replace('/\(.*$/', '', $request->awal);
        $periode_awal = $carbon->parse($periode_awal)->format('Y-m');
        $periode_peramalan = preg_replace('/\(.*$/', '', $request->akhir);
        $periode_peramalan = $carbon->parse($periode_peramalan)->format('Y-m');
        //cari selisih bulan
        $timeStart = strtotime($periode_awal);
        $timeEnd = strtotime($periode_peramalan);
        // Menambah bulan ini + semua bulan pada tahun sebelumnya
        $date_diff = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
        // menghitung selisih bulan
        $date_diff += date("m",$timeEnd)-date("m",$timeStart);

        $obat = $request->obat;

        if ($date_diff <= 0) {
            $message = 'periode awal harus lebih kecil';
        } elseif ($date_diff < 2) {
            $message = 'minimal 2 periode';
        }  else {
            $periode = $this->get_periode($periode_awal, $periode_peramalan);
            $x = $this->getData($date_diff, $obat, $periode);
            if (count($x[0]) != $date_diff) {
                $message = 'data periode tidak lengkap mohon lengkapi data dahulu';
            } else {
                $beta = $this->getBeta();
                for ($i=0; $i < count($beta); $i++) { 
                    for ($j=0; $j < $date_diff; $j++) {
                        if ($j == 0) {
                            $res[$i][$j]['X'] = $x[0][$j];
                            $res[$i][$j]['F'] = 0; // nilai F index 0
                            $res[$i][$j]['e'] = 0; // nilai e index 0
                            $res[$i][$j]['E'] = 0; // nilai E index 0
                            $res[$i][$j]['EA'] = 0; // nilai EA index 0
                            $res[$i][$j]['alpha'] = 0; // nilai alpha index 0
                            $res[$i][$j]['beta'] = $beta[$i];
                            $res[$i][$j]['PE'] = 0; // nilai PE index 0
                        } elseif ($j == 1) {
                            $res[$i][$j]['X'] = $x[0][$j];
                            $res[$i][$j]['F'] = $x[0][0]; // nilai F index 1
                            $res[$i][$j]['e'] = $this->countESmal($res[$i][$j]['X'], $res[$i][$j]['F']); // nilai e index 1
                            $res[$i][$j]['E'] = $this->countEBig($res[$i][$j]['e'], $beta[$i], 0); // nilai EA index 1
                            $res[$i][$j]['EA'] = $this->countEA($res[$i][$j]['e'], $beta[$i], 0); // nilai E index 1
                            $res[$i][$j]['alpha'] = $beta[$i]; // nilai alpha index 1
                            $res[$i][$j]['beta'] = $beta[$i];
                            $res[$i][$j]['PE'] = $this->countPE($res[$i][$j]['X'], $res[$i][$j]['F']); // nilai PE index 1
                        } else {
                            $res[$i][$j]['X'] = $x[0][$j];
                            $res[$i][$j]['F'] = $this->countF($res[$i][$j-1]['alpha'], $res[$i][$j-1]['X'], $res[$i][$j-1]['F']);
                            $res[$i][$j]['e'] = $this->countESmal($res[$i][$j]['X'], $res[$i][$j]['F']);
                            $res[$i][$j]['E'] = $this->countEBig($res[$i][$j]['e'], $beta[$i], $res[$i][$j-1]['E']);
                            $res[$i][$j]['EA'] = $this->countEA($res[$i][$j]['e'], $beta[$i], $res[$i][$j-1]['EA']);
                            $res[$i][$j]['alpha'] = $this->countAlfa($res[$i][$j-1]['E'], $res[$i][$j-1]['EA']);
                            $res[$i][$j]['beta'] = $beta[$i];
                            $res[$i][$j]['PE'] = $this->countPE($res[$i][$j]['X'], $res[$i][$j]['F']);
                        } 
                        $x[1][$j] = $res[$i][$j]['F'];
                    }
                    $mape[$i] = $this->countMAPE($res[$i]);
                    $min_data[$mape[$i]] = $res[$i];
                }
                $show_data['beta'] = min($mape);
                $show_data['data'] = $min_data[min($mape)];
                $show_data['hasil'] = $this->countF($show_data['data'][($date_diff-1)]['alpha'], $show_data['data'][($date_diff-1)]['X'], $show_data['data'][($date_diff-1)]['F']);
                $message = 'berhasil masuk';
            }
        }

        $chart['datasets'][0]['label'] = 'Data Aktual';
        $chart['datasets'][0]['data'] = $x[0];
        $chart['datasets'][0]['data'][count($x[1])] = 0;
        $chart['datasets'][0]['borderColor'] = $this->random_color();
        $chart['datasets'][0]['backgroundColor'] = '#6777ef';
        // backgroundColor: '#6777ef',
        $chart['datasets'][0]['pointRadius'] = 5;
        $chart['datasets'][0]['tension'] = 0.5;
        // $chart['datasets'][0]['pointBackgroundColor'] = 'transparent';
        // $chart['datasets'][0]['pointHoverBackgroundColor'] = $this->random_color();
        $chart['datasets'][1]['label'] = 'Data Peramalan';
        $chart['datasets'][1]['data'] = $x[1];
        $chart['datasets'][1]['data'][count($x[1])] = $show_data['hasil'];
        $chart['datasets'][1]['borderColor'] = $this->random_color();
        // $chart['datasets'][1]['backgroundColor'] = $this->random_color();
        $chart['datasets'][1]['pointRadius'] = 5;
        $chart['datasets'][1]['tension'] = 0.5;
        // $chart['datasets'][1]['pointBackgroundColor'] = 'transparent';
        // $chart['datasets'][1]['pointHoverBackgroundColor'] = $this->random_color();

        $chart['label'] = $this->stringPeriode($periode);

        // $chart['datasets'][0]['data'] = $resData;
        // $chart['datasets'][0]['borderColor'] = $this->random_color();
        // $chart['datasets'][0]['backgroundColor'] = $this->random_color();
        // $chart['datasets'][0]['pointRadius'] = 5;
        // $chart['datasets'][0]['tension'] = 0.5;
        // return response($chart);
        return response()->json([
            // 'bulan'=> $bulan, 
            // 'tahun'=> $tahun, 
            'obat'=>$obat, 
            // 'data_bulan' => $date_diff,
            // 'data' => $x,
            // 'result'=> $res,
            // 'mape' => $mape,
            // 'show_data' => $show_data,
            'message' => $message,
            'chart' => $chart

        ]);
    }

    public function getData($counter, $obat, $periode)
    {
        $res = [];
        $carbon = new Carbon();
        for ($i=0; $i < $counter; $i++) { 
            $data = MedicineDetail::where('medicine_id', $obat)
                                ->whereYear('created_at', $carbon->create($periode[$i])->format('Y'))
                                ->whereMonth('created_at', $carbon->create($periode[$i])->format('m'))
                                ->get('quantity');
            if (count($data) != 0) {
                $res[0][$i] = $this->countMultipleStockData($data);
                $res[1][$i] = 0;
            } 
        }
        return $res;
    }

    public function getDataLabel(){
        return json_encode($this->getMonth());
    }

    public function getDataTahun()
    {
        return $this->getYear();
    }

    public function getDataObat()
    {
        return response(Medicine::all());
    }

    private function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    private function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }    

    private function getYear()
    {
        $min = MedicineDetail::select()->min('created_at');
        $max = MedicineDetail::select()->max('created_at');
        return $this->getOneYears($this->minYear($min),$this->maxYear($max));
    }

    private function getMonth()
    {
        return $this->getMonths();
    }

    private function get_periode($periode_awal, $periode_peramalan) //get periode peramalan
	{
		//cari selisih bulan
		$timeStart = strtotime($periode_awal);
		$timeEnd = strtotime($periode_peramalan);
		// Menambah bulan ini + semua bulan pada tahun sebelumnya
		$months = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
		// menghitung selisih bulan
		$months += date("m",$timeEnd)-date("m",$timeStart);

		//set periode
		$periode = array();
		for ($i = 0; $i <= $months; $i++)
		{
			$periode[] = date('Y-m', strtotime('+'.$i.' month', strtotime($periode_awal)));
		}

		return $periode;
	}

    private function stringPeriode($periode)
    {
        $carbon = new Carbon();
        $res = [];
        for ($i=0; $i < count($periode); $i++) { 
            $res[$i] = $carbon->create($periode[$i])->isoFormat('MMMM Y');
        }
        $res[count($periode)] = $carbon->create($periode[count($periode)-1])->add(1, 'month')->isoFormat('MMMM Y');
        return $res;
    }
}
