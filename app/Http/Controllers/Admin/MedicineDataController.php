<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Medicine;
use App\Models\MedicineDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MedicineDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $obat = Medicine::all();
        return view('admin.ingoing.create1', [
            'title' => 'ingoing',
            'subtitle' => 'create',
            'active' => 'permintaan',
            'obat' => $obat
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'obat' => 'required',
            'jumlah' => 'required|numeric',
            'bulan' => 'required',
            'tahun' => 'required',
            
            // 'expired_date' => 'required|date'
        ], [
            'obat.required' => 'Obat harap diisi',
            'jumlah.required' => 'Tipe harus diisi',
            'jumlah.numeric' => 'satuan miligram harus angka',
            'bulan.required' => 'Bulan harus diisi',
            'tahun.required' => 'Tahun harus diisi',
        ]);

        $dt = Carbon::now();
        $date = new Carbon($request->tahun.'-'.$request->bulan.'-'.$dt->day.' 00:00:00');

        MedicineDetail::insert([
            'medicine_id' => $request->obat,
            'quantity' => $request->jumlah,
            'expired_date' => $date,
            'status' => '0',
            'created_at' => $date
        ]);

        return redirect()->route('ingoing.index')->with('success_msg', 'Tambah Data Obat Masuk berhasil ditambah ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
