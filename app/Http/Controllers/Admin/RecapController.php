<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RecapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.recap.index', [
            'title' => 'rekap',
            'subtitle' => '',
            'active' => 'rekap',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $carbon = new Carbon();

            $obat = '';
            if (!empty($request->get('obat'))) {
                $obat = $request->get('obat');
            }

            $bulan = '';
            $tahun = '';
            if (!empty($request->get('awal'))) {
                $bulan = $carbon->createFromFormat('m-Y', $request->get('awal'))->format('m');
                $tahun = $carbon->createFromFormat('m-Y', $request->get('awal'))->format('Y');
            }

            $data = TransactionDetail::leftJoin('transactions', 'transaction_details.transaction_id', '=', 'transactions.id')
                        ->leftJoin('medicines', 'transaction_details.medicine_id', '=', 'medicines.id')
                        ->leftJoin('medicine_types', 'medicines.type_id', '=', 'medicine_types.id')
                        ->where('transactions.invoice_number', '!=', '-')
                        ->where('transaction_details.medicine_id', $obat)
                        ->whereYear('transactions.created_at', "$tahun")
                        ->whereMonth('transactions.created_at', "$bulan")
                        ->get(['transaction_details.*', 'transactions.*', 'medicines.name as nama_obat', 'medicine_types.name as nama_tipe']);

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('name', function($row){
                    $obat = '';
                    $obat = $row->nama_obat . ' ' . $row->nama_tipe . ' ' . $row->weight;
                    return $obat;
                })
                ->addColumn('unitmasuk', function($row){
                    $unitmasuk = '';
                    if ($row->type_id == 1) {
                        $unitmasuk = $row->quantity;
                    } else {
                        $unitmasuk = '-';
                    }
                    return $unitmasuk;
                })
                // ->addColumn('jumlahmasuk', function($row){
                //     $jumlahmasuk = '';
                //     if ($row->type_id == 1) {
                //         $jumlahmasuk = $row->price;
                //     } else {
                //         $jumlahmasuk = '-';
                //     }
                //     return $jumlahmasuk;
                // })
                ->addColumn('unitkeluar', function($row){
                    $unitkeluar = '';
                    if ($row->type_id == 2) {
                        $unitkeluar = $row->quantity;
                    }
                    else {
                        $unitkeluar = '-';
                    }
                    return $unitkeluar;
                })
                // ->addColumn('jumlahkeluar', function($row){
                //     $jumlahkeluar = '';
                //     if ($row->type_id == 2) {
                //         $jumlahkeluar = $row->price;
                //     } else {
                //         $jumlahkeluar =  '-';
                //     }
                //     return $jumlahkeluar;
                // })
                // ->addColumn('sisa', function($row){
                //     $stok = 0;
                //     return $stok;
                // })
                ->make(true);
        }
    }
}
