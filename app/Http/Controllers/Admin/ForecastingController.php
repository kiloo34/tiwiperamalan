<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Medicine;
use App\Models\MedicineDetail;
use Illuminate\Http\Request;
use App\Traits\Helpers;
use App\Traits\Arsess;
use App\Traits\Mape;
use Carbon\Carbon;

class ForecastingController extends Controller
{
    use Helpers, Arsess, Mape;

    public function index()
    {
        return view('admin.forecasting.index', [
            'title' => 'peramalan',
            'subtitle' => '',
            'active' => 'peramalan',
        ]);
    }

    public function getDataTahun(Request $request)
    {
        if($request->ajax()){ 
            $min = MedicineDetail::select()->min('created_at');
            $max = MedicineDetail::select()->max('created_at');
            return response($this->getYears($this->minYear($min),$this->maxYear($max)));
        } else {
            return response()->json(['text'=>'only ajax request']);
        }
    }

    public function getDataBulan(Request $request)
    {
        if($request->ajax()){ 
            return response($this->getMonths());
        } else {
            return response()->json(['text'=>'only ajax request']);
        }
    }

    public function getDataObat(Request $request)
    {
        if($request->ajax()){
            return response(Medicine::with('tipe')->get());
        }else {
            return response()->json(['text'=>'only ajax request']);
        }
    }

    public function calcultaeForecasting(Request $request)
    {   
        $x = [];
        $message = '';
        $res = [];
        $mape = [];
        $show_data = [];
        
        $carbon = new Carbon();
        $periode_awal = preg_replace('/\(.*$/', '', $request->awal);
        $periode_awal = $carbon->parse($periode_awal)->format('Y-m');
        $periode_peramalan = preg_replace('/\(.*$/', '', $request->akhir);
        $periode_peramalan = $carbon->parse($periode_peramalan)->format('Y-m');

        //cari selisih bulan
        $timeStart = strtotime($periode_awal);
        $timeEnd = strtotime($periode_peramalan);
        // Menambah bulan ini + semua bulan pada tahun sebelumnya
        $date_diff = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
        // menghitung selisih bulan
        $date_diff += date("m",$timeEnd)-date("m",$timeStart);

        $obat = $request->obat;

        if ($date_diff <= 0) {
            $message = 'periode awal harus lebih kecil';
        } elseif ($date_diff < 2) {
            $message = 'minimal 2 periode';
        }  else {
            $periode = $this->get_periode($periode_awal, $periode_peramalan);
            $x = $this->getData($date_diff, $obat, $periode);
            if (count($x) != ($date_diff+1)) {
                $message = 'data periode tidak lengkap mohon lengkapi data dahulu';
            } else {
                $beta = $this->getBeta();
                for ($i=0; $i < count($beta); $i++) { 
                    for ($j=0; $j <= $date_diff; $j++) {
                        if ($j == 0) {
                            $res[$i][$j]['X'] = $x[$j];
                            $res[$i][$j]['F'] = 0; // nilai F index 0
                            $res[$i][$j]['e'] = 0; // nilai e index 0
                            $res[$i][$j]['E'] = 0; // nilai E index 0
                            $res[$i][$j]['EA'] = 0; // nilai EA index 0
                            $res[$i][$j]['alpha'] = 0; // nilai alpha index 0
                            $res[$i][$j]['beta'] = $beta[$i];
                            $res[$i][$j]['PE'] = 0; // nilai PE index 0
                        } elseif ($j == 1) {
                            $res[$i][$j]['X'] = $x[$j];
                            $res[$i][$j]['F'] = $x[0]; // nilai F index 1
                            $res[$i][$j]['e'] = $this->countESmal($res[$i][$j]['X'], $res[$i][$j]['F']); // nilai e index 1
                            $res[$i][$j]['E'] = $this->countEBig($res[$i][$j]['e'], $beta[$i], 0); // nilai EA index 1
                            $res[$i][$j]['EA'] = $this->countEA($res[$i][$j]['e'], $beta[$i], 0); // nilai E index 1
                            $res[$i][$j]['alpha'] = $beta[$i]; // nilai alpha index 1
                            $res[$i][$j]['beta'] = $beta[$i];
                            $res[$i][$j]['PE'] = $this->countPE($res[$i][$j]['X'], $res[$i][$j]['F']); // nilai PE index 1
                        } else {
                            $res[$i][$j]['X'] = $x[$j];
                            $res[$i][$j]['F'] = $this->countF($res[$i][$j-1]['alpha'], $res[$i][$j-1]['X'], $res[$i][$j-1]['F']);
                            $res[$i][$j]['e'] = $this->countESmal($res[$i][$j]['X'], $res[$i][$j]['F']);
                            $res[$i][$j]['E'] = $this->countEBig($res[$i][$j]['e'], $beta[$i], $res[$i][$j-1]['E']);
                            $res[$i][$j]['EA'] = $this->countEA($res[$i][$j]['e'], $beta[$i], $res[$i][$j-1]['EA']);
                            $res[$i][$j]['alpha'] = $this->countAlfa($res[$i][$j-1]['E'], $res[$i][$j-1]['EA']);
                            $res[$i][$j]['beta'] = $beta[$i];
                            $res[$i][$j]['PE'] = $this->countPE($res[$i][$j]['X'], $res[$i][$j]['F']);
                        } 
                    }
                    $mape[$i] = $this->countMAPE($res[$i]);
                    $min_data[$mape[$i]] = $res[$i];
                }

                $stringBulanTahun = preg_replace('/\(.*$/', '', $request->akhir);
                $show_data['beta'] = min($mape);
                $show_data['data'] = $min_data[min($mape)];
                $show_data['hasil'] = $this->countF($show_data['data'][($date_diff-1)]['alpha'], $show_data['data'][($date_diff-1)]['X'], $show_data['data'][($date_diff-1)]['F']);
                $show_data['bulan'] = $carbon->parse($stringBulanTahun)->add(1, 'month')->format('F');
                $show_data['tahun'] = $carbon->parse($stringBulanTahun)->add(1, 'month')->format('Y');

                $message = 'berhasil masuk';
            }
        }

        return response()->json([
            // 'bulan'=> $bulan, 
            // 'tahun'=> $tahun, 
            'obat'=>$obat, 
            'data_bulan' => $date_diff,
            // 'data' => $x,
            'result'=> $res,
            'mape' => $mape,
            'show_data' => $show_data,
            'message' => $message

        ]);
    }

    private function get_periode($periode_awal, $periode_peramalan) //get periode peramalan
	{
		//cari selisih bulan
		$timeStart = strtotime($periode_awal);
		$timeEnd = strtotime($periode_peramalan);
		// Menambah bulan ini + semua bulan pada tahun sebelumnya
		$months = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
		// menghitung selisih bulan
		$months += date("m",$timeEnd)-date("m",$timeStart);

		//set periode
		$periode = array();
		for ($i = 0; $i <= $months; $i++)
		{
			$periode[] = date('Y-m', strtotime('+'.$i.' month', strtotime($periode_awal)));
		}

		return $periode;
	}

    public function getData($counter, $obat, $periode)
    {
        $res = [];
        $carbon = new Carbon();
        for ($i=0; $i <= $counter; $i++) { 
            $data = MedicineDetail::where('medicine_id', $obat)
                                ->whereYear('created_at', $carbon->create($periode[$i])->format('Y'))
                                ->whereMonth('created_at', $carbon->create($periode[$i])->format('m'))
                                ->get('quantity');
            if (count($data) != 0) {
                $res[$i] = $this->countMultipleStockData($data);
            } 
        }
        return $res;
    }
}
