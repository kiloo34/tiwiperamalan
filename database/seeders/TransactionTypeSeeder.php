<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('transaction_Types')->truncate();
        \DB::table('transaction_Types')->insert([
            [
                'name' => 'ingoing'
            ],
            [
                'name' => 'outgoing'
            ],
        ]);
    }
}
