<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TransactionDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('transaction_details')->truncate();
        \DB::table('transaction_details')->insert([
            [
                'transaction_id' => 1,
                'medicine_id' => 2,
                'price' => '5000',
                'quantity' => 10,
                'expired_date' => Carbon::now()->subMonth(2)->format('Y-m-d H:i:s'),
            ],
            [
                'transaction_id' => 2,
                'medicine_id' => 1,
                'price' => '5000',
                'quantity' => 10,
                'expired_date' => Carbon::now()->subMonth(2)->format('Y-m-d H:i:s'),
            ],
            [
                'transaction_id' => 3,
                'medicine_id' => 3,
                'price' => '6000',
                'quantity' => 10,
                'expired_date' => Carbon::now()->subMonth(2)->format('Y-m-d H:i:s'),
            ],
            [
                'transaction_id' => 4,
                'medicine_id' => 4,
                'price' => '1000',
                'quantity' => 10,
                'expired_date' => Carbon::now()->subMonth(2)->format('Y-m-d H:i:s'),
            ],
            [
                'transaction_id' => 5,
                'medicine_id' => 1,
                'price' => '5000',
                'quantity' => 10,
                'expired_date' => Carbon::now()->subMonth(2)->format('Y-m-d H:i:s'),
            ],
            [
                'transaction_id' => 6,
                'medicine_id' => 1,
                'price' => '0',
                'quantity' => 2,
                'expired_date' => Carbon::now()->subMonth(3)->format('Y-m-d H:i:s'),
            ],
            [
                'transaction_id' => 6,
                'medicine_id' => 2,
                'price' => '0',
                'quantity' => 4,
                'expired_date' => Carbon::now()->subMonth(4)->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}


// $table->unsignedBigInteger('transaction_id');
// $table->foreign('transaction_id')->references('id')->on('transactions')->onUpdate('cascade')->onDelete('cascade');
// $table->unsignedBigInteger('medicine_id');
// $table->foreign('medicine_id')->references('id')->on('medicines')->onUpdate('cascade')->onDelete('cascade');
// $table->string('price');
// $table->string('quantity');
// $table->date('expired_date');