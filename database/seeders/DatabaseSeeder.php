<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            RoleSeeder::class,
            UserSeeder::class,
            MedicineTypeSeeder::class,
            MedicineSeeder::class,
            MedicineDetailSeeder::class,
            TransactionTypeSeeder::class,
            TransactionStatusSeeder::class,
            TransactionSeeder::class,
            TransactionDetailSeeder::class
        ]);
    }
}
