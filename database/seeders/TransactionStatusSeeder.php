<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TransactionStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('transaction_statuses')->truncate();
        \DB::table('transaction_statuses')->insert([
            [
                'name' => 'menunggu pembayaran'
            ],
            [
                'name' => 'diproses'
            ],
            [
                'name' => 'dikirim'
            ],
            [
                'name' => 'selesai'
            ],
            [
                'name' => 'batal'
            ],
            [
                'name' => 'keranjang'
            ],
        ]);
    }
}
