<?php

namespace Database\Seeders;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Seeder;

class MedicineDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('medicine_details')->truncate();

        $date = rand(1,30);
        $month = rand(1,12);

        // $data = ;
        // $res = new DateTime($data);
        
        \DB::table('medicine_details')->insert([
            // ================================================Paracetamol Sirup======================================
            // =======================================2018=======================================
            [
                'medicine_id'   => 2,
                'quantity'      => 180,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 200,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 160,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 195,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 183,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 203,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 188,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 200,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 170,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 190,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 210,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 185,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-12-'.rand(1,30).' 00:00:00'))
            ],
            // ========================================2019===========================================
            [
                'medicine_id'   => 2,
                'quantity'      => 205,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 228,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 195,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 232,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 190,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 210,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 165,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 215,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 185,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 170,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 208,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 180,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-12-'.rand(1,30).' 00:00:00'))
            ],
            // =================================================2020====================================
            [
                'medicine_id'   => 2,
                'quantity'      => 210,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 225,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 200,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 220,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 180,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 210,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 190,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 215,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 165,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 195,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 205,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 185,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-12-'.rand(1,30).' 00:00:00'))
            ],
            // ===========================================2021======================================
            [
                'medicine_id'   => 2,
                'quantity'      => 210,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 190,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 195,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 215,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 170,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 200,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 215,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 175,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 205,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 198,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 178,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 2,
                'quantity'      => 170,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-12-'.rand(1,30).' 00:00:00'))
            ],

            // ================================Paracetamol Tablet=======================================
            // =======================================2018=======================================
            [
                'medicine_id'   => 1,
                'quantity'      => 4000,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4700,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5000,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5400,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4800,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5000,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5300,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4900,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4700,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-12-'.rand(1,30).' 00:00:00'))
            ],
            // ========================================2019===========================================
            [
                'medicine_id'   => 1,
                'quantity'      => 4900,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5400,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5800,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5200,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4700,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5100,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5000,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4800,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5300,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-12-'.rand(1,30).' 00:00:00'))
            ],
            // =================================================2020====================================
            [
                'medicine_id'   => 1,
                'quantity'      => 4800,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5100,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5500,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5400,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4900,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5100,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5300,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5000,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4800,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-12-'.rand(1,30).' 00:00:00'))
            ],
            // ===========================================2021======================================
            [
                'medicine_id'   => 1,
                'quantity'      => 5500,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4600,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4200,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4800,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5100,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5500,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5600,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 4800,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 5200,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 1,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-12-'.rand(1,30).' 00:00:00'))
            ],
        ]);

        \DB::table('medicine_details')->insert([
            // ================================================Asam Mefenamat======================================
            // =======================================2018=======================================
            [
                'medicine_id'   => 3,
                'quantity'      => 7800,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 7200,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6800,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5500,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6100,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6700,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5750,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6200,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5200,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5800,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5700,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-12-'.rand(1,30).' 00:00:00'))
            ],
            // ========================================2019===========================================
            [
                'medicine_id'   => 3,
                'quantity'      => 7800,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 7200,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6800,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5500,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6100,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6700,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5750,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6200,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5200,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5800,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5700,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-12-'.rand(1,30).' 00:00:00'))
            ],
            // =================================================2020====================================
            [
                'medicine_id'   => 3,
                'quantity'      => 7800,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 7500,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6200,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5500,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6100,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5800,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5700,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5200,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5000,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6500,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-12-'.rand(1,30).' 00:00:00'))
            ],
            // ===========================================2021======================================
            [
                'medicine_id'   => 3,
                'quantity'      => 7800,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6800,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5500,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6100,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6300,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5800,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5200,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5500,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 6000,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 3,
                'quantity'      => 5700,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-12-'.rand(1,30).' 00:00:00'))
            ],
        ]);

        \DB::table('medicine_details')->insert([
            // ================================================Ibuprofen======================================
            // =======================================2018=======================================
            [
                'medicine_id'   => 4,
                'quantity'      => 75,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 100,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 80,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 77,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 85,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 90,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 78,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 82,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 95,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 78,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 80,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 98,
                'expired_date'  => Carbon::create('2018-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2018-12-'.rand(1,30).' 00:00:00'))
            ],
            // ========================================2019===========================================
            [
                'medicine_id'   => 4,
                'quantity'      => 80,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 98,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 78,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 75,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 100,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 83,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 82,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 76,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 98,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 85,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 90,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 100,
                'expired_date'  => Carbon::create('2019-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2019-12-'.rand(1,30).' 00:00:00'))
            ],
            // =================================================2020====================================
            [
                'medicine_id'   => 4,
                'quantity'      => 78,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 100,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 85,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 98,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 80,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 76,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 92,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 75,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 80,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 95,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 80,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 78,
                'expired_date'  => Carbon::create('2020-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2020-12-'.rand(1,30).' 00:00:00'))
            ],
            // ===========================================2021======================================
            [
                'medicine_id'   => 4,
                'quantity'      => 100,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-01-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 85,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-02-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 98,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-03-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 78,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-04-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 80,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-05-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 75,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-06-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 95,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-07-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 87,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-08-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 90,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-09-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 93,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-10-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 79,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-11-'.rand(1,30).' 00:00:00'))
            ],
            [
                'medicine_id'   => 4,
                'quantity'      => 92,
                'expired_date'  => Carbon::create('2021-'.rand(1,12).'-'.rand(1,30).' 00:00:00'),
                'created_at'    => Carbon::create(date('2021-12-'.rand(1,30).' 00:00:00'))
            ],
        ]);
    }
}
