<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MedicineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('medicines')->truncate();
        \DB::table('medicines')->insert([
            [
                'name'      => 'Paracetamol',
                'weight'    => '500',
                'stock'     => 10,
                'type_id'   => 1
            ],
            [
                'name'      => 'Paracetamol',
                'weight'    => '120/5',
                'stock'     => 10,
                'type_id'   => 2
            ],
            [
                'name'      => 'Asam Mefenamat',
                'weight'    => '500',
                'stock'     => 10,
                'type_id'   => 3
            ],
            [
                'name'      => 'Ibuprofen',
                'weight'    => '100/5',
                'stock'     => 10,
                'type_id'   => 4
            ],
        ]);
    }
}
