<?php

namespace Database\Seeders;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date   = new DateTime();

        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('transactions')->truncate();
        \DB::table('transactions')->insert([
            [
                'invoice_number' => 'INV' . rand(pow(10, 3-1), pow(10, 3)-1) . implode("", explode('-', $date->format('Y-m-d-H-i-s'))),
                'status_id' => 2,
                'type_id' => 1,
                'created_by' => 1,
                'created_at' => Carbon::now()->subMonth(1)->format('Y-m-d H:i:s')
            ],
            [
                'invoice_number' => 'INV' . rand(pow(10, 3-1), pow(10, 3)-1) . implode("", explode('-', $date->format('Y-m-d-H-i-s'))),
                'status_id' => 2,
                'type_id' => 1,
                'created_by' => 1,
                'created_at' => Carbon::now()->subMonth(2)->format('Y-m-d H:i:s')
            ],
            [
                'invoice_number' => 'INV' . rand(pow(10, 3-1), pow(10, 3)-1) . implode("", explode('-', $date->format('Y-m-d-H-i-s'))),
                'status_id' => 3,
                'type_id' => 1,
                'created_by' => 1,
                'created_at' => Carbon::now()->subMonth(1)->format('Y-m-d H:i:s')
            ],
            [
                'invoice_number' => 'INV' . rand(pow(10, 3-1), pow(10, 3)-1) . implode("", explode('-', $date->format('Y-m-d-H-i-s'))),
                'status_id' => 4,
                'type_id' => 2,
                'created_by' => 1,
                'created_at' => Carbon::now()->subMonth(3)->format('Y-m-d H:i:s')
            ],
            [
                'invoice_number' => 'INV' . rand(pow(10, 3-1), pow(10, 3)-1) . implode("", explode('-', $date->format('Y-m-d-H-i-s'))),
                'status_id' => 5,
                'type_id' => 1,
                'created_by' => 1,
                'created_at' => Carbon::now()->subMonth(1)->format('Y-m-d H:i:s')
            ],
            [
                'invoice_number' => '-',
                'status_id' => 6,
                'type_id' => 2,
                'created_by' => 1,
                'created_at' => Carbon::now()->subMonth(1)->format('Y-m-d H:i:s')
            ],
        ]);
    }
}
