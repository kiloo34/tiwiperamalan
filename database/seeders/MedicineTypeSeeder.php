<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MedicineTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('medicine_types')->truncate();
        \DB::table('medicine_types')->insert([
            [
                'name' => 'Tablet'
            ],
            [
                'name' => 'Sirup'
            ],
            [
                'name' => 'Kapsul'
            ],
            [
                'name' => 'Suspensi'
            ],
        ]);
    }
}
